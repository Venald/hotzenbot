/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.sys.database;

import core.sync.mutex;

import d2sqlite3;

/**
 * Thread-safe wrapper for database requests. This ensures that the
 * database is not locked when trying to access it. Do NOT use
 * database.execute!
 */
auto hotzenbot_exec(A...)(Database* db, string request, A a)
{
    return db.execute(request, a);
}

__gshared Mutex db_mutex;

void start_transaction(Database* db)
{
    if (db_mutex is null)
    {
        db_mutex = new Mutex();
    }

    db_mutex.lock_nothrow();
    db.begin();
}

void commit_transaction(Database* db)
{
    db.commit();
    db_mutex.unlock_nothrow();
}

void rollback_transaction(Database* db)
{
    db.rollback();
    db_mutex.unlock_nothrow();
}
