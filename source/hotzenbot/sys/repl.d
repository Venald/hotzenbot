/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.sys.repl;

import core.thread;

import std.algorithm;
import std.conv;
import std.datetime;
import std.file;
import std.socket;
import std.string;
import std.parallelism;

import hotzenbot.bot.gitlabhelp;
import hotzenbot.bot.timerthread;
import hotzenbot.commands.command;
import hotzenbot.commands.expressionparser;
import hotzenbot.commands.replcontext;
import hotzenbot.commands.pipe;
import hotzenbot.sys.logging;
import hotzenbot.sys.transport;

import d2sqlite3;

struct ReplCommand
{
    string usage;
    int arg_count; // Set to -1 to accept any number of arguments
    bool requires_join;
    void function(ReplState*, string) run;
}

struct ReplState
{
    string channel;
    string transport_name;
    ReplThread* repl_thread;
    void delegate(string) send_message;

    string access_token()
    {
        return repl_thread.access_token;
    }

    string client_id()
    {
        return repl_thread.client_id;
    }

    Transport[string] transports()
    {
        return repl_thread.transports;
    }

    Transport current_transport()
    {
        return this.transports[transport_name];
    }

    Database* database()
    {
        return repl_thread.database;
    }

    bool is_joined()
    {
        return channel && !channel.empty;
    }
}

void run_cd_command(ReplState* state, string arg)
{
    auto transport_channel_pair = arg.split("/");
    if (transport_channel_pair.length != 2)
    {
        state.send_message("usage: cd <transport/#channel>\n");
        return;
    }

    if (transport_channel_pair[0] in state.transports)
    {
        state.transport_name = transport_channel_pair[0];
        state.channel = transport_channel_pair[1];
        log_info(LoggingScope.Repl, "Switching to " ~ state.transport_name ~ "/" ~ state.channel);
    }
    else
    {
        state.send_message(transport_channel_pair[0] ~ ": No such transport\n");
    }
}

void run_join_command(ReplState* state, string arg)
{
    auto transport_channel_pair = arg.split("/");
    if (transport_channel_pair.length != 2)
    {
        state.send_message("usage: join <transport/#channel>\n");
        return;
    }

    state.transport_name = transport_channel_pair[0];
    state.channel = transport_channel_pair[1];

    state.current_transport.join_channel(state.channel);
    log_info(LoggingScope.Repl, "Joining " ~ state.transport_name ~ "/" ~ state.channel);
}

void run_part_command(ReplState* state, string arg)
{
    state.current_transport.part_channel(state.channel);
    state.transport_name = null;
    state.channel = null;
}

void run_setprefix_command(ReplState* state, string arg)
{
    auto result = state.current_transport.set_call_prefix(state.channel, arg[0]);
    if (result.is_error)
    {
        state.send_message("ERR : " ~ result.err_msg ~ "\n");
    }
    else
    {
        state.send_message(result.result ~ "\n");
    }
}

void run_say_command(ReplState* state, string arg)
{
    state.current_transport.say(state.channel, arg);
}

void run_addperiodic_command(ReplState* state, string arg)
{
    auto words = arg.split();
    if (words.length < 3)
    {
        state.send_message("ERR : usage: addperiodic <interval> <code>");
        return;
    }

    try
    {
        Duration d;
        int t = words[0].parse!int;
        switch (words[1])
        {
        case "h":
        case "hrs":
        case "hours":
            d = hours(t);
            break;
        case "m":
        case "mins":
        case "minutes":
            d = minutes(t);
            break;
        case "days":
        case "d":
            d = days(t);
            break;
        default:
            state.send_message("ERR : Unknown unit of time or unit is not suitable for periodic timers.\n");
            return;
        }

        auto code = words[2..$].join(" ");
        auto parser = ExpressionParser(code);
        auto parse_result = parser.parse();
        if (parse_result.is_error)
        {
            state.send_message("ERR : Error evaluating command to execute: " ~ parse_result.err_msg);
            return;
        }

        auto timer = PeriodicTimer(
            state.transport_name,
            state.channel,
            code,
            d,
            Clock.currTime + d,
            69);
        add_periodictimer(timer);
        state.send_message("Added periodic timer\n");
    }
    catch (Exception e)
    {
        state.send_message("ERR : Want an integer for the interval\n");
    }
}

void run_addcmd_command(ReplState* state, string arg)
{
    auto words = arg.split(" ");
    if (words.length <= 1)
    {
        state.send_message("usage: addcmd <command-name> <command-code>\n");
        return;
    }

    string command_name = words[0];
    string command_code = arg.chompPrefix(command_name ~ " ");

    auto result = state.database.add_command(command_name, command_code);
    if (result.is_error)
    {
        state.send_message("ERR : " ~ result.err_msg ~ "\n");
    }
    else
    {
        state.send_message("Added command " ~ command_name ~ ".\n");
        update_gitlab_help_page();
    }
}

void run_addalias_command(ReplState* state, string arg)
{
    auto words = arg.split(" ");
    if (words.length != 2)
    {
        state.send_message("usage: addalias <command-name> <alias-name>\n");
        return;
    }

    string command_name = words[0];
    string alias_name = words[1];

    auto result = state.database.add_alias(command_name, alias_name);
    if (result.is_error)
    {
        state.send_message("ERR : " ~ result.err_msg ~ "\n");
    }
    else
    {
        state.send_message("Added alias " ~ alias_name ~ " -> " ~ command_name ~ ".\n");
        update_gitlab_help_page();
    }
}

void run_delcmd_command(ReplState* state, string arg)
{
    auto result = state.database.delete_command_by_name(arg);
    if (result.is_error)
    {
        state.send_message("ERR : " ~ result.err_msg ~ "\n");
    }
    else
    {
        state.send_message("Deleted command " ~ arg ~ ".\n");
        update_gitlab_help_page();
    }
}

void run_eval_command(ReplState* state, string arg)
{
    ReplContext ctx = new ReplContext(state.database, state.access_token, state.client_id);
    auto parser = ExpressionParser(arg);
    auto parse_result = parser.parse();

    if (parse_result.is_error)
    {
        state.send_message("ERR : parse error: " ~ parse_result.err_msg ~ "\n");
        return;
    }

    auto eval_result = parse_result.result.evaluate(ctx);
    if (eval_result.is_error)
    {
        state.send_message("ERR : " ~ eval_result.err_msg ~ "\n");
    }
    else
    {
        state.send_message(eval_result.result ~ "\n");
    }
}

class ReplThread : Thread
{
    this(Database* database)
    {
        super(&repl_thread);
        this.database = database;
    }

    void request_exit()
    {
        this.should_exit = true;
    }

    void repl_thread()
    {
        enum hotzensocket = "/var/tmp/hotzenbot.sock";
        try
        {
            if (exists(hotzensocket))
            {
                remove(hotzensocket);
            }

            log_info(LoggingScope.Repl, "Creating socket");
            listening_socket = new Socket(AddressFamily.UNIX, SocketType.STREAM);
            listening_socket.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, 1);
            auto address = new UnixAddress(hotzensocket);
            log_info(LoggingScope.Repl, "Binding socket");
            listening_socket.bind(address);

            listening_socket.listen(2);
            log_info(LoggingScope.Repl, "Listening on socket at /var/tmp/hotzenbot.sock.\n" ~
                     "You can connect to it with `nc -U " ~ hotzensocket ~ "`");

            scope(success) {
                log_info(LoggingScope.Repl, "Closing and shutting down sockets");
                listening_socket.shutdown(SocketShutdown.BOTH);
                listening_socket.close();
            }

            while (!this.should_exit)
            {
                auto connection = listening_socket.accept();
                log_info(LoggingScope.Repl, "Received connection");
                auto new_worker = task!handle_connection(this, connection);
                new_worker.executeInNewThread();
            }
        }
        catch (Exception e)
        {
            import std.conv : to;

            log_fatal(LoggingScope.Repl, "Repl thread crashed with: " ~ e.to!string);
        }
    }

    void add_transport_layer(string name, Transport transport)
    {
        if (name in transports)
        {
            log_error(LoggingScope.Repl, "Transport already registered");
        }
        else
        {
            log_info(LoggingScope.Repl, "Transport `" ~ name ~ "`registered");
            transports[name] = transport;
        }
    }

    static void handle_connection(ref ReplThread repl_thread, Socket connection_socket)
    {
        char[512] buffer;

        void send_message(string message)
        {
            connection_socket.send(cast(void[])(message));
        }

        ReplCommand[string] repl_commands =
            [ "cd"          : ReplCommand("cd <transport/#channel>"              , 1, false, &run_cd_command),
              "join"        : ReplCommand("join <transport/#channel>"            , 1, false, &run_join_command),
              "part"        : ReplCommand("part"                                 , 0, true, &run_part_command),
              "setprefix"   : ReplCommand("setprefix <prefix>"                   , 1, true, &run_setprefix_command),
              "say"         : ReplCommand("say <msg>"                            , -1, true, &run_say_command),
              "addcmd"      : ReplCommand("addcmd <command-name> <command-code>" , -1, false, &run_addcmd_command),
              "delcmd"      : ReplCommand("delcmd <command-name>"                , 1, false, &run_delcmd_command),
              "addalias"    : ReplCommand("addalias <command-name> <alias-name>" , 2, false, &run_addalias_command),
              "eval"        : ReplCommand("eval <expression>"                    , -1, false, &run_eval_command),
              "addperiodic" : ReplCommand("addperiodic <interval> <code>"        , -1, true, &run_addperiodic_command)
            ];
        ReplState repl_state = ReplState(null, null, &repl_thread, &send_message);

        scope(success) {
            connection_socket.shutdown(SocketShutdown.BOTH);
            connection_socket.close();
            log_info(LoggingScope.Repl, "Connection closed");
        }

        send_message("Welcome to the hotzenbot REPL\n");

        while (true)
        {
            buffer.fill('\0');
            send_message(repl_state.transport_name ~ "/" ~ repl_state.channel ~ "> ");

            ulong read_bytes = connection_socket.receive(buffer);
            if (read_bytes == 0 || read_bytes == Socket.ERROR)
            {
                log_error(LoggingScope.Repl, "Unable to read from socket");
                return;
            }

            string line = cast(string)(buffer[0..read_bytes-1]).dup.strip;
            log_info(LoggingScope.Repl, "Received command: " ~ line);

            if (line == "quit")
            {
                send_message("Bye\n");
                break;
            }

            if (line == "help")
            {
                foreach (repl_command; repl_commands) {
                    send_message(repl_command.usage);
                    send_message("\n");
                }
                continue;
            }

            auto words = line.split(" ");
            if (words.empty)
            {
                continue;
            }

            if (words[0] in repl_commands)
            {
                auto command = repl_commands[words[0]];
                if (command.arg_count != -1 && words.length != command.arg_count + 1)
                {
                    send_message("usage: " ~ command.usage ~ "\n");
                    continue;
                }

                if (command.requires_join && !repl_state.is_joined)
                {
                    send_message(words[0] ~ " requires a join\n");
                    continue;
                }

                if (command.arg_count == 0)
                    command.run(&repl_state, null);
                else
                    command.run(&repl_state, words[1..$].join(" "));
            }
            else
            {
                send_message("Unknown command `" ~ words[0] ~ "`\n");
            }
        }
    }

    Socket listening_socket;
    bool should_exit = false;
    Transport[string] transports;
    string client_id;
    string access_token;
    Database* database;
}
