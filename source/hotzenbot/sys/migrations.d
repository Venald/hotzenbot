/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.sys.migrations;

import std.algorithm;
import std.conv : to;
import std.range;
import std.container.array;
import std.stdio;
import std.string;
import std.digest.sha;

import core.stdc.stdlib;

import d2sqlite3;

import hotzenbot.sys.database;
import hotzenbot.sys.logging;

class Migrations
{
    Array!Migration kgbotka_migrations;
    Array!Migration hotzenbot_migrations;

    this()
    {
        kgbotka_migrations.insert(
           [
               Migration("CREATE TABLE Command ( id INTEGER PRIMARY KEY, code TEXT NOT NULL );"),
               Migration("CREATE TABLE CommandName ( name TEXT NOT NULL, commandId INTEGER NOT NULL REFERENCES Command(id) ON DELETE CASCADE, UNIQUE(name) ON CONFLICT REPLACE );"),
               Migration("CREATE TABLE TwitchRoles ( id INTEGER PRIMARY KEY, name TEXT NOT NULL UNIQUE );"),
               Migration("CREATE TABLE TwitchUserRoles ( userId TEXT NOT NULL, roleId INTEGER NOT NULL REFERENCES TwitchRoles(id) ON DELETE CASCADE, UNIQUE(userId, roleId) ON CONFLICT IGNORE );"),
               Migration("CREATE TABLE FridayVideo ( id INTEGER PRIMARY KEY, submissionText TEXT NOT NULL, submissionTime DATETIME NOT NULL, authorId TEXT NOT NULL, authorDisplayName TEXT NOT NULL, watchedAt DATETIME );"),
               Migration("CREATE TABLE TwitchLog ( id INTEGER PRIMARY KEY, channel TEXT NOT NULL, senderTwitchId TEXT NOT NULL, senderTwitchName TEXT NOT NULL, senderTwitchDisplayName TEXT, senderTwitchRoles TEXT NOT NULL, senderTwitchBadgeRoles TEXT NOT NULL, message TEXT NOT NULL, messageTime DATETIME DEFAULT (datetime('now')) NOT NULL )"),
               Migration("CREATE TABLE Markov ( event1 TEXT NOT NULL, event2 TEXT NOT NULL, n INTEGER NOT NULL, UNIQUE (event1, event2) ON CONFLICT REPLACE ); CREATE INDEX markov_event1_index ON Markov (event1);"),
               Migration("ALTER TABLE Command ADD COLUMN user_cooldown_ms INTEGER NOT NULL DEFAULT 0;"),
               Migration("CREATE TABLE CommandLog ( userTwitchId TEXT, userDiscordId TEXT, commandId INTEGER NOT NULL, commandArgs TEXT NOT NULL, timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP );"),
               Migration("CREATE TABLE AsciifyUrlCache( url TEXT NOT NULL, image TEXT NOT NULL, UNIQUE (url) ON CONFLICT REPLACE );"),
               Migration("CREATE TABLE BttvEmotes ( name TEXT NOT NULL, channel TEXT DEFAULT NULL, imageUrl TEXT NOT NULL );"),
               Migration("CREATE TABLE FfzEmotes ( name TEXT NOT NULL, channel TEXT DEFAULT NULL, imageUrl TEXT NOT NULL );"),
               Migration("CREATE TABLE DiscordLog ( id INTEGER PRIMARY KEY, guildId TEXT, channelId TEXT NOT NULL, senderDiscordId TEXT NOT NULL, message TEXT NOT NULL, messageTime DATETIME DEFAULT (datetime('now')) NOT NULL )"),
               Migration("CREATE TABLE Settings ( name TEXT NOT NULL, value TEXT NOT NULL )"),
               Migration("CREATE TABLE JoinedTwitchChannels ( name TEXT NOT NULL, UNIQUE (name) ON CONFLICT IGNORE )"),
               Migration("ALTER TABLE Command ADD COLUMN times INT NOT NULL DEFAULT 0;"),
               Migration("ALTER TABLE TwitchLog RENAME TO TwitchLogOld;"),
               Migration("CREATE TABLE TwitchLog ( id INTEGER PRIMARY KEY, channel TEXT NOT NULL, senderTwitchId TEXT, senderTwitchName TEXT NOT NULL, senderTwitchDisplayName TEXT, senderTwitchRoles TEXT NOT NULL, senderTwitchBadgeRoles TEXT NOT NULL, message TEXT NOT NULL, messageTime DATETIME DEFAULT (datetime('now')) NOT NULL);"),
               Migration("INSERT INTO TwitchLog (id, channel, senderTwitchId, senderTwitchName, senderTwitchDisplayName, senderTwitchRoles, senderTwitchBadgeRoles, message, messageTime) SELECT id, channel, senderTwitchId, senderTwitchName, senderTwitchDisplayName, senderTwitchRoles, senderTwitchBadgeRoles, message, messageTime FROM TwitchLogOld;"),
               Migration("DROP TABLE TwitchLogOld;"),
               Migration("ALTER TABLE DiscordLog RENAME TO DiscordLogOld;"),
               Migration("CREATE TABLE DiscordLog ( id INTEGER PRIMARY KEY, guildId TEXT, channelId TEXT NOT NULL, senderDiscordId TEXT, senderDiscordDisplayName TEXT, message TEXT NOT NULL, messageTime DATETIME DEFAULT (datetime('now')) NOT NULL );"),
               Migration("INSERT INTO DiscordLog (id, guildId, channelId, senderDiscordId, senderDiscordDisplayName, message, messageTime) SELECT id, guildId, channelId, senderDiscordId, NULL, message, messageTime FROM DiscordLogOld;"),
               Migration("DROP TABLE DiscordLogOld;"),
               Migration("CREATE TABLE RoleEmojiAssoc ( emojiId TEXT NOT NULL, roleId INTEGER NOT NULL, msgId INTEGER NOT NULL, UNIQUE(emojiId, msgId) );"),
               Migration("ALTER TABLE Command ADD COLUMN argsRegex TEXT NOT NULL DEFAULT '(.*)';"),
               Migration("CREATE TABLE xkcd ( num INTEGER UNIQUE, title TEXT, img TEXT, alt TEXT, transcript TEXT );"),
               Migration("CREATE TABLE IF NOT EXISTS xkcd_tf_idf ( term TEXT, freq INTEGER, num INTEGER );"),
               Migration("ALTER TABLE JoinedTwitchChannels ADD COLUMN channelCommandPrefix TEXT NOT NULL DEFAULT '!';"),
               ]
           );

        hotzenbot_migrations.insert(
            [
                Migration("CREATE TABLE IF NOT EXISTS IrcLog (id PRIMARY KEY, senderIrcNickName TEXT NOT NULL, senderIrcChannel TEXT NOT NULL, senderIrcNetwork TEXT NOT NULL, message TEXT NOT NULL, messageTime DATETIME DEFAULT (datetime('now')) NOT NULL);"),
                Migration("ALTER TABLE CommandLog ADD COLUMN userIrcAddress TEXT;"),
                Migration("CREATE TABLE JoinedIRCChannels ( name TEXT NOT NULL, network TEXT NOT NULL, channelCommandPrefix TEXT NOT NULL DEFAULT '!', UNIQUE(name, network) ON CONFLICT IGNORE);"),
                Migration("CREATE TABLE IF NOT EXISTS IrcUserRoles ( userNetwork TEXT NOT NULL, userNickname TEXT NOT NULL, userAddress TEXT NOT NULL, roleId INTEGER NOT NULL REFERENCES TwitchRoles(id) ON DELETE CASCADE, UNIQUE(userNetwork, userNickname, userAddress, roleId) ON CONFLICT IGNORE);"),
                Migration("CREATE TABLE IF NOT EXISTS TwitchVariables ( id PRIMARY KEY, twitchChannelName TEXT NOT NULL, variableName TEXT NOT NULL, variableText TEXT, UNIQUE(twitchChannelName, variableName) ON CONFLICT REPLACE);"),
                Migration("CREATE TABLE IF NOT EXISTS IrcVariables ( id PRIMARY KEY, ircNetworkName TEXT NOT NULL, ircChannelName TEXT NOT NULL, variableName TEXT NOT NULL, variableText TEXT, UNIQUE(ircNetworkName, ircChannelName, variableName) ON CONFLICT REPLACE );"),
                Migration("CREATE INDEX IF NOT EXISTS markovIdx ON Markov ( event1, event2 );"),
                Migration("CREATE TABLE IF NOT EXISTS CalcVariables (calcVarName TEXT NOT NULL, calcVarValue REAL, calcVarTwitchId TEXT, calcVarIrcNick TEXT, calcVarIrcAddress TEXT, calcVarIrcNetwork TEXT, UNIQUE(calcVarTwitchId, calcVarName) ON CONFLICT REPLACE, UNIQUE(calcVarName, calcVarIrcNick, calcVarIrcAddress, calcVarIrcNetwork) ON CONFLICT REPLACE);"),
                Migration("CREATE TABLE IF NOT EXISTS Timers(id INTEGER PRIMARY KEY, network TEXT NOT NULL, channel TEXT NOT NULL, userName TEXT NOT NULL, message TEXT NOT NULL, dueTime DATETIME NOT NULL );"),
                Migration("CREATE TABLE IF NOT EXISTS Quotes (quoteNetwork TEXT NOT NULL, quoteChannel TEXT NOT NULL, quoteDate TEXT NOT NULL, quoteMessage TEXT NOT NULL);"),
                Migration("CREATE INDEX IF NOT EXISTS quoteIdx ON Quotes ( quoteNetwork, quoteChannel );"),
                Migration("CREATE TABLE IF NOT EXISTS PeriodicTimers (id INTEGER PRIMARY KEY, network TEXT NOT NULL, channel TEXT NOT NULL, expression TEXT NOT NULL, duration INTEGER NOT NULL, nextTriggerTime TEXT NOT NULL);")
             ]
        );
    }

    enum MigrationType
    {
        Hotzenbot,
        KGBotka
    }

    void check_and_apply_migrations(Database* database)
    {
        database.begin();
        scope(exit) database.commit();

        log_info(LoggingScope.System, "Checking migrations table");
        create_migration_table_if_needed(database);

        log_info(LoggingScope.System, "Performing kgbotka migrations if necessary");
        perform_migrations(database, kgbotka_migrations, MigrationType.KGBotka);

        log_info(LoggingScope.System, "Performing hotzenbot migrations if necessary");
        perform_migrations(database, hotzenbot_migrations, MigrationType.Hotzenbot);
    }

    private void perform_migrations(Database* database, Array!Migration migrations, MigrationType type)
    {
        ResultRange applied_migrations;

        final switch (type)
        {
        case MigrationType.Hotzenbot:
        {
            applied_migrations = database.hotzenbot_exec("SELECT queryDgst from HotzenbotMigrations;");
        } break;
        case MigrationType.KGBotka:
        {
            applied_migrations = database.hotzenbot_exec("SELECT queryDgst from Migrations;");
        } break;
        }

        uint max_i = 0;

        uint i = 0;
        foreach (applied_migration; applied_migrations.map!(x => x.peek!string(0)))
        {
            if (i >= migrations.length)
            {
                stderr.writeln("More migrations have been applied than the bot actually knows about.");
                core.stdc.stdlib.abort();
            }

            if (hash_of_query(migrations[i].sql) != applied_migration)
            {
                stderr.writeln("Inconsistent migration state. Migrations at index " ~ i.to!string ~ " collide.");
                core.stdc.stdlib.abort();
            }

            max_i = i;
            ++i;
        }

        if (max_i + 1 == migrations.length)
        {
            log_info(LoggingScope.System, "Migration check successful. Database is up-to-date.");
            return;
        }

        /*
         * This weird hack is necessary, because if no migrations have
         * ever been applied, i would be initialized with 1 and skip
         * the first migration.
         */
        for (i = max_i ? max_i + 1 : 0; i < migrations.length; ++i)
        {
            log_debug(LoggingScope.System, "Applying migration: " ~ migrations[i].sql);
            migrations[i].apply(database, type);
        }

        log_info(LoggingScope.System, "Migrations applied successfully. Database is up-to-date again.");
    }

    private void create_migration_table_if_needed(Database* database)
    {
        database.hotzenbot_exec("CREATE TABLE IF NOT EXISTS Migrations ( id INTEGER PRIMARY KEY, migrationQuery TEXT NOT NULL );");

        /*
         * HACK WARNING! Cannot check if a column exists in
         * sqlite....so let's just throw an exception and decide upon
         * that whether to upgrade the db 4HEad
         */

        try
        {
            auto rows = database.hotzenbot_exec("SELECT queryDgst FROM Migrations;");
        }
        catch (Exception e)
        {
            /*
             * Here we behave differently
             */
            do_upgrade_for_hotzenbot(database);
        }
    }

    private void do_upgrade_for_hotzenbot(Database* database)
    {
        writeln("WARNING!");
        writeln("Hotzenbot needs to upgrade the Migration table. Those actions will NOT affect the compatibility to kgbotka,\n" ~
                "but in the future kgbotka may not know about some migrations that hotzenbot applied.");
        string answer;
        do
        {
            write("Do you want to upgrade the database? [y|N] ");
            answer = readln().strip;
        } while (!(answer == "y" || answer == "Y" || answer == "n" || answer == "N" || answer == ""));

        if (answer.toLower == "n" || answer.empty)
        {
            stderr.writeln("Database upgrade aborted");
            core.stdc.stdlib.abort();
        }

        /*
         * Insert the dgst column
         */
        database.hotzenbot_exec("ALTER TABLE Migrations ADD COLUMN queryDgst TEXT NOT NULL DEFAULT '';");

        /*
         * Perform actual upgrade
         */
        auto applied_migrations = database.hotzenbot_exec("SELECT id, migrationQuery FROM Migrations;");

        auto statement = database.prepare("UPDATE Migrations SET queryDgst=:dgst WHERE id=:id;");
        log_info(LoggingScope.System, "Peforming migration table upgrade");

        foreach (migration; applied_migrations)
        {
            string query = migration.peek!string(1);
            int id = migration.peek!int(0);

            string dgst = hash_of_query(query);
            statement.bind(":dgst", dgst);
            statement.bind(":id", id);

            statement.execute();
            statement.reset();
        }

        /*
         * Add HotzenbotMigrations table
         */
        database.hotzenbot_exec("CREATE TABLE IF NOT EXISTS HotzenbotMigrations ( id INTEGER PRIMARY KEY, migrationQuery TEXT NOT NULL, queryDgst TEXT NOT NULL DEFAULT '' );");

        log_info(LoggingScope.System, "Migration table has been upgraded successfully.");
    }

    static string hash_of_query(string query)
    {
        string result;
        foreach (c; query)
        {
            import std.ascii;
            if (!c.isWhite)
            {
                result ~= c;
            }
        }

        ubyte[32] hash = sha256Of(result);

        return hash.toHexString.dup;
    }

    struct Migration
    {
        string sql;

        void apply(Database* database, MigrationType type)
        {
            log_debug(LoggingScope.System, "Applying migration: " ~ sql);
            database.hotzenbot_exec(sql);
            final switch (type)
            {
            case MigrationType.Hotzenbot: {
                database.hotzenbot_exec("INSERT INTO HotzenbotMigrations (migrationQuery, queryDgst) VALUES (:query, :dgst);",
                                 sql, hash_of_query(sql));
            } break;
            case MigrationType.KGBotka:
            {
                database.hotzenbot_exec("INSERT INTO Migrations (migrationQuery, queryDgst) VALUES (:query, :dgst);",
                                 sql, hash_of_query(sql));
            } break;
            }
        }
    }
}
