/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.sys.result;

struct Result(V)
{
    bool is_error;
    string err_msg;
    V result;

    static Result fail(string msg)
    {
        Result!V foo;
        foo.is_error = true;
        foo.err_msg = msg;
        return foo;
    }

    static Result ok(V v)
    {
        Result!V foo;
        foo.is_error = false;
        foo.result = v;
        return foo;
    }
}

alias EvalResult = Result!string;
