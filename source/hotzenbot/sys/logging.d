/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.sys.logging;

import std.algorithm;
import std.ascii;
import std.concurrency;
import std.container.dlist;
import std.conv : to;
import std.datetime.systime;
import std.stdio;

enum LoggingScope
{
    System,
    Twitch,
    IRC,
    Repl,
    Markov,
    Timers
}

string to_string(LoggingScope scp)
{
    final switch (scp)
    {
    case LoggingScope.System:
        return "system";
    case LoggingScope.Twitch:
        return "twitch";
    case LoggingScope.IRC:
        return "irc";
    case LoggingScope.Repl:
        return "repl";
    case LoggingScope.Markov:
        return "markov";
    case LoggingScope.Timers:
        return "timers";
    }
}

__gshared Tid logger_thread_id;
__gshared bool logger_should_exit;

enum LoggingLevel { Warn, Info, Debug, Error, Fatal };

struct LoggingMessage
{
    string msg;
    LoggingScope log_scope;
    LoggingLevel level;

    string toString()
    {
        string message;

        final switch (level)
        {
        case LoggingLevel.Warn:
            message ~= "[WARN] ";
            break;
        case LoggingLevel.Info:
            message ~= "[INFO] ";
            break;
        case LoggingLevel.Debug:
            message ~= "[DBUG] ";
            break;
        case LoggingLevel.Error:
            message ~= "[ERR ] ";
            break;
        case LoggingLevel.Fatal:
            message ~= "[FATAL] ";
            break;
        }

        return message ~ Clock.currTime.to!string ~ " [" ~ log_scope.to_string ~ "] " ~ msg;
    }
}

void logger_thread_worker(Tid parent)
{
    auto log_file = File("hotzenbot.log", "a");
    while (!logger_should_exit)
    {
        receive(
            (LoggingMessage message) {
                auto s = message.toString;

                debug
                {
                    if (message.level != LoggingLevel.Debug)
                    {
                        log_file.writeln(s);
                        log_file.flush();
                    }
                    stderr.writeln(s);
                }
                else
                {
                    if (message.level != LoggingLevel.Debug)
                    {
                        log_file.writeln(s);
                        log_file.flush();
                        stderr.writeln(s);
                    }
                }

            });
    }
}

void init_logger()
{
    logger_should_exit = false;
    logger_thread_id = spawn(&logger_thread_worker, thisTid);
}

void kill_logger()
{
    logger_should_exit = true;
}

void log_warn(LoggingScope log_scope, string msg)
{
    send(logger_thread_id, LoggingMessage(msg, log_scope, LoggingLevel.Warn));
}

void log_debug(LoggingScope log_scope, string msg)
{
    send(logger_thread_id, LoggingMessage(msg, log_scope, LoggingLevel.Debug));
}

void log_info(LoggingScope log_scope, string msg)
{
    send(logger_thread_id, LoggingMessage(msg, log_scope, LoggingLevel.Info));
}

void log_error(LoggingScope log_scope, string msg)
{
    send(logger_thread_id, LoggingMessage(msg, log_scope, LoggingLevel.Error));
}

void log_fatal(LoggingScope log_scope, string msg)
{
    send(logger_thread_id, LoggingMessage(msg, log_scope, LoggingLevel.Fatal));
}
