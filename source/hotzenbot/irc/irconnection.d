/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.irc.ircconnection;

import std.algorithm;
import std.base64;
import std.container.dlist;
import std.conv : to;
import std.exception;
import std.functional;
import std.json;
import std.outbuffer;
import std.socket;
import std.stdio;
import std.string;
import std.typecons;

import core.sync.mutex;
import core.thread;

import dialect;

import hotzenbot.bot.messagehandler;
import hotzenbot.irc.bufferedsocket;
import hotzenbot.irc.ircreadthread;
import hotzenbot.irc.ircwritethread;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;
import hotzenbot.sys.transport;

class IRCConnection : Transport
{
    this(JSONValue config, BotMessageHandler *handler, string network)
    {
        this.config = config;
        this.client.nickname = config["nick"].str;
        this.server.address = config["host"].str;
        this.port = cast(ushort)(config["port"].integer);
        this.message_handler = handler;
        this.parser = IRCParser(client, server);
        this.network = network;

        if ("use_sasl" in config)
        {
            this.use_sasl = config["use_sasl"].boolean;
            if (this.use_sasl)
            {
                if ("sasl_password" in config)
                {
                    this.sasl_password = config["sasl_password"].str;
                }
                else
                {
                    log_fatal(LoggingScope.IRC, "Cannot authenticate with SASL when no password is provided. "~
                              "Declare the 'sasl_password' field for the network " ~ network ~ ".");
                    enforce(0, "Configuration failed");
                }
            }
        }
    }

    void connect()
    {
        log_info(LoggingScope.IRC, "Resolving addresses");
        auto addresses = getAddress(this.server.address, this.port);

        if (addresses.length == 0)
        {
            log_error(LoggingScope.IRC, "Could not resolve address.");
            return;
        }

        log_info(LoggingScope.IRC, "Choosing first address: " ~ addresses[0].to!string);

        socket = new BufferedTcpSocket(addresses[0]);

        if (!socket.isAlive)
        {
            log_error(LoggingScope.IRC, "Could not connect to " ~ server.address);
            return;
        }

        auto user = config["username"].str;
        auto nick = config["nick"].str;

        Thread.sleep(dur!"seconds"(5));

        if (this.use_sasl)
        {
            sasl_auth(nick, user);
        }
        else
        {
            log_info(LoggingScope.IRC, "Authenticating");
            send_nick_and_user(nick, user);
        }
        // Start the write thread
        write_thread = new IRCWriteThread(&socket);
        write_thread.start();

        read_thread = new IRCReadThread(&socket, &parser, toDelegate(&on_message_received));
        read_thread.start();
    }

    void send_nick_and_user(string nick, string user)
    {
        socket.send(cast(const(void)[])("NICK " ~ nick ~ "\r\n"));
        socket.send(cast(const(void)[])("USER " ~ user ~ " * * :" ~ user ~ "\r\n"));
    }

    void sasl_auth(string nick, string user)
    {
        log_info(LoggingScope.IRC, "Authenticating with SASL");
        socket.send(cast(const(void)[])("CAP REQ :sasl\r\n"));
        send_nick_and_user(nick, user);
        socket.send(cast(const(void)[])("AUTHENTICATE PLAIN\r\n"));

        while (true)
        {
            string line = socket.read_line();
            try
            {
                auto event = parser.toIRCEvent(line);
                if (event.type == IRCEvent.Type.SASL_AUTHENTICATE)
                {
                    log_info(LoggingScope.IRC, "Performing SASL Auth.");
                    break;
                }
                else
                {
                    log_info(LoggingScope.IRC, "Waiting for SASL ACK: " ~ event.content);
                }
            }
            catch (Exception e)
            {
                log_error(LoggingScope.IRC, "Unable to parse IRC message: " ~ e.to!string);
            }
        }

        OutBuffer auth_buf = new OutBuffer();
        auth_buf.write(user);
        auth_buf.write(cast(ubyte)0);
        auth_buf.write(user);
        auth_buf.write(cast(ubyte)0);
        auth_buf.write(sasl_password);
        string sasl_line = Base64.encode(auth_buf.toBytes);
        socket.send("AUTHENTICATE " ~ sasl_line ~ "\r\n");
        try
        {
            for(;;)
            {
                string line = socket.read_line;
                auto event = parser.toIRCEvent(line);
                if (event.type == IRCEvent.Type.RPL_LOGGEDIN)
                {
                    socket.send(cast(const(void)[])("CAP END\r\n"));
                    continue;
                }
                else if (event.type == IRCEvent.Type.RPL_SASLSUCCESS)
                {
                    log_info(LoggingScope.IRC, "SASL Authentication succeeded");
                    return;
                }
                else
                {
                    log_error(LoggingScope.IRC, "Unable to authenticate with SASL: " ~ event.content);
                    return;
                }
            }

        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to parse IRC message: " ~ e.to!string);
        }
    }

    void auto_join_channels()
    {
        log_info(LoggingScope.IRC, "auto-joining registered IRC channels");
        auto channel_names = message_handler.database.hotzenbot_exec("SELECT name FROM JoinedIRCChannels WHERE network = :network;", this.network);

        foreach (channel_name; channel_names)
        {
            join_channel(channel_name.peek!string(0));
        }
    }

    void on_message_received(IRCEvent event)
    {
        message_handler.on_irc_event(event, this.client.nickname, network, write_thread);
    }

    void send_message(string message)
    {
        write_thread.push_message(message);
    }

    void join_channel(string channel)
    {
        send_message("JOIN " ~ channel ~ "\r\n");
    }

    void part_channel(string channel)
    {
        send_message("PART " ~ channel ~ "\r\n");
    }

    void say(string channel, string message)
    {
        send_message("PRIVMSG " ~ channel ~ " : " ~ message ~ "\r\n");
    }

    Result!string set_call_prefix(string channel, char prefix)
    {
        try
        {
            message_handler.database.hotzenbot_exec("UPDATE JoinedIRCChannels SET channelCommandPrefix=:prefix WHERE name=:name AND network=:network ;",
                                             prefix.to!string, channel, this.network);
            return Result!string.ok("Updated call prefix for channel " ~ network ~ "/" ~ channel);
        }
        catch (Exception e)
        {
            auto err_msg = "Failed to set command prefix for irc channel " ~ network ~ "/" ~ channel ~ ": " ~ e.to!string;
            log_error(LoggingScope.IRC, err_msg);
            return Result!string.fail(err_msg);
        }
    }

    void exit()
    {
        read_thread.request_stop();
        write_thread.request_stop();
        read_thread.join();
        write_thread.join();
    }

    JSONValue config;
    BufferedTcpSocket socket;
    IRCClient client;
    IRCServer server;
    IRCParser parser;
    bool is_connected = false,
        use_sasl = false;
    IRCWriteThread write_thread;
    IRCReadThread read_thread;
    BotMessageHandler* message_handler;
    ushort port;
    string network, sasl_password;
}
