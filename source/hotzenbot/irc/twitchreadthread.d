/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.irc.twitchreadthread;

import core.thread;
import dialect;
import std.conv : to;
import std.socket;
import std.stdio;
import std.string;

import hotzenbot.bot.botversion;
import hotzenbot.irc.bufferedsocket;
import hotzenbot.sys.logging;

class TwitchReadThread : Thread
{
    this(BufferedTcpSocket* socket, IRCParser* parser, void delegate(IRCEvent) event_callback) @safe nothrow
    {
        this.socket = socket;
        this.parser = parser;
        this.callback = event_callback;
        super(&read_loop);
    }

    public string get_raw_message()
    {
        return socket.read_line();
    }

    private IRCEvent next_event()
    {
        while (true)
        {
            string message = get_raw_message();
            if (message is null || message.empty) return IRCEvent();

            IRCEvent event;
            try
            {
                event = this.parser.toIRCEvent(message);
            }
            catch (Exception e)
            {
                log_fatal(LoggingScope.Twitch, "Unable to parse IRC message: " ~ e.to!string);
                continue;
            }

            switch (event.type)
            {
            case IRCEvent.Type.CTCP_PING:
            {
                log_info(LoggingScope.Twitch, "Got CTCP PING request");
                socket.send(cast(const(void)[])("NOTICE " ~ event.sender.nickname ~ ":\x01PING " ~ event.content ~ "\x01\r\n"));
                continue;
            }
            case IRCEvent.Type.CTCP_VERSION:
            {
                log_info(LoggingScope.Twitch, "Got CTCP VERSION request");
                socket.send(cast(const(void)[])("NOTICE " ~ event.sender.nickname ~ ":\x01VERSION hotzenbot " ~ bot_version()~ "\x01\r\n"));
                continue;
            }
            case IRCEvent.Type.CTCP_CLIENTINFO:
            {
                log_info(LoggingScope.Twitch, "Got CTCP CLIENTINFO request");
                socket.send(cast(const(void)[])("NOTICE " ~ event.sender.nickname ~ ":\x01CLIENTINFO VERSION PING\x01\r\n"));
                continue;
            }
            case IRCEvent.Type.PING:
            {
                log_info(LoggingScope.Twitch, "Received Ping: " ~ event.to!string);
                log_info(LoggingScope.Twitch, "Sending Pong: PONG :tmi.twitch.tv");
                socket.send(cast(const(void)[])("PONG :tmi.twitch.tv\r\n"));

                continue;
            }
            case IRCEvent.Type.NOTICE:
            {
                log_info(LoggingScope.Twitch, "TWITCH-NOTICE " ~ event.content);
                continue;
            }
            case IRCEvent.Type.RPL_MOTDSTART:
            case IRCEvent.Type.RPL_MOTD:
            case IRCEvent.Type.RPL_ENDOFMOTD:
            {
                log_info(LoggingScope.Twitch, "TWITCH-MOTD "  ~ event.content);
                continue;
            }
            default:
                return event;
            }
        }
    }

    void request_stop()
    {
        should_exit = true;
        socket.should_exit = true;
    }

    void read_loop()
    {
        log_info(LoggingScope.Twitch, "Starting receive loop");
        socket.setOption(SocketOptionLevel.SOCKET,
                         SocketOption.RCVTIMEO, dur!"seconds"(1));
        while (!should_exit)
        {
            try
            {
                auto event = next_event();
                if (event.type != IRCEvent.Type.UNSET)
                    callback(event);
            }
            catch (Exception e)
            {
                log_error(LoggingScope.Twitch, "Unable to read event from socket: " ~ e.to!string);
            }
        }

        log_info(LoggingScope.Twitch, "Twitch read loop exited by request.");
    }

    void delegate(IRCEvent) callback;
    BufferedTcpSocket* socket;
    IRCParser* parser;
    shared bool should_exit = false;
}
