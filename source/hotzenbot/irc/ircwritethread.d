/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.irc.ircwritethread;

import core.sync.mutex;
import core.thread;
import std.algorithm;
import std.container.dlist;
import std.conv : to;
import std.socket;
import std.stdio;

import hotzenbot.irc.bufferedsocket;
import hotzenbot.sys.logging;

class IRCWriteThread : Thread
{
    this(BufferedTcpSocket* socket) @safe nothrow
    {
        queue_lock = new shared Mutex();
        this.socket = socket;
        super(&write_loop);
    }

    private void write_loop()
    {
        while (!should_exit)
        {
            try
            {
                if (!outgoing_messages[].empty)
                {
                    queue_lock.lock_nothrow();
                    foreach (message; outgoing_messages)
                    {
                        auto line = cast(const(void)[])(message);
                        socket.send(line);
                    }

                    outgoing_messages.clear();
                    queue_lock.unlock_nothrow();
                }
                else
                    Thread.sleep(200.msecs);
            }
            catch (Exception e)
            {
                log_fatal(LoggingScope.System, "Sending exception: " ~ e.to!string);
            }
        }
    }

    void request_stop()
    {
        should_exit = true;
    }

    void push_message(string message)
    {
        queue_lock.lock_nothrow();
        outgoing_messages.insertBack(message);
        queue_lock.unlock_nothrow();
    }

    DList!string outgoing_messages;
    shared Mutex queue_lock;
    BufferedTcpSocket* socket;
    shared bool should_exit = false;
}
