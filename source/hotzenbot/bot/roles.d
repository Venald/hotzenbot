/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.roles;

import d2sqlite3;

import dialect;

import std.algorithm;
import std.array;
import std.container.dlist;
import std.conv : to;

import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

interface Role
{
    string name();
    bool is_authority();
    bool is_maintainer();
    bool is_trusted();
    bool is_ignored();
}

class CustomRole : Role
{
    string role_name;
    int id;

    string name()
    {
        return this.role_name;
    }

    bool is_authority()
    {
        return ["authority", "maintainer"].canFind(role_name);
    }

    bool is_maintainer()
    {
        return role_name == "maintainer";
    }

    bool is_trusted()
    {
        return ["trusted", "maintainer", "authority"].canFind(role_name);
    }

    bool is_ignored()
    {
        return role_name == "ignored";
    }

    this(int id, string role_name)
    {
        this.id = id;
        this.role_name = role_name;
    }
}

DList!Role custom_roles_of_twitch_user(Database* database, int user_twitch_id)
{
    try
    {
        auto query_result = database.hotzenbot_exec("SELECT ur.roleId, r.name " ~
                                                    "FROM TwitchUserRoles ur " ~
                                                    "INNER JOIN TwitchRoles r " ~
                                                    "ON ur.roleId = r.id " ~
                                                    "WHERE ur.userId = :userId;", user_twitch_id);

        return DList!Role(query_result.map!(row => new CustomRole(row.peek!int(0),
                                                                  row.peek!string(1)))); // Some
                                                                                         // LISP
                                                                                         // for
                                                                                         // ya
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to fetch custom roles of user wih id " ~
                  user_twitch_id.to!string ~ ": " ~ e.to!string);
        return DList!Role();
    }
}

DList!Role custom_roles_of_irc_user(Database* database, string network, string user_address, string user_nick)
{
    try
    {
        auto query_result = database.hotzenbot_exec("SELECT ur.roleId, r.name " ~
                                                    "FROM IrcUserRoles ur " ~
                                                    "INNER JOIN TwitchRoles r " ~
                                                    "ON ur.roleId = r.id " ~
                                                    "WHERE ur.userNetwork = :user_network " ~
                                                    "  AND ur.userNickname = :user_nick " ~
                                                    "  AND ur.userAddress  = :user_address;",
                                                    network, user_nick, user_address);

        return DList!Role(query_result.map!(row => new CustomRole(row.peek!int(0),
                                                                  row.peek!string(1))));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.IRC, "Unable to fetch custom roles of irc user " ~
                  network ~ "/" ~ user_nick ~ "@" ~ user_address ~ ": " ~ e.to!string);
        return DList!Role();
    }
}

class TwitchBadgeRole : Role
{
    string badge_role_name;

    string name()
    {
        return badge_role_name;
    }

    override bool is_maintainer()
    {
        return false;
    }

    override bool is_authority()
    {
        return ["broadcaster", "moderator"].canFind(badge_role_name);
    }

    override bool is_trusted()
    {
        return ["broadcaster","moderator", "vip", "subscriber"].canFind(badge_role_name);
    }

    override bool is_ignored()
    {
        return false;
    }

    this(string role_name)
    {
        this.badge_role_name = role_name.split("/")[0];
    }
}

DList!Role badge_roles_of_twitch_event(IRCEvent event)
{
    if (event.sender.badges.empty || event.sender.badges == "*")
    {
        return DList!Role();
    }

    return DList!Role(
        event.sender.badges
        .split(",")
        .map!(badge => new TwitchBadgeRole(badge)));
}

Result!int get_twitch_role_id_by_name(Database* db, string role_name)
{
    try
    {
        auto query_result = db.hotzenbot_exec("SELECT id FROM TwitchRoles WHERE name = :name;", role_name);
        if (query_result.empty)
        {
            return Result!int.fail("No such role");
        }

        return Result!int.ok(query_result.front.peek!int(0));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to fetch role id" ~ e.to!string);
        return Result!int.fail("Unable to fetch role id");
    }
}

Result!string add_twitch_role(Database* db, string role_name)
{
    try
    {
        db.hotzenbot_exec("INSERT INTO TwitchRoles (name) VALUES (:name);", role_name);
        return Result!string.ok("Added role");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to add twitch role: " ~ e.to!string);
        return Result!string.fail("Unable to add role");
    }
}
