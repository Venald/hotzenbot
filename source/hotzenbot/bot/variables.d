/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.variables;

import hotzenbot.commands.context;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.conv : to;

import d2sqlite3;

EvalResult get_var(Context ctx, string var)
{
    TwitchContext tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        return get_twitch_variable(tctx.database, tctx.channel, var);
    }

    IRCContext ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        return get_irc_variable(ictx.database, ictx.network, ictx.channel, var);
    }

    return EvalResult.fail("Variables are not supported in a non-Twitch context.");
}

EvalResult set_var(Context ctx, string var, string var_text)
{
    TwitchContext tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        return set_twitch_variable(tctx.database, tctx.channel, var, var_text);
    }

    IRCContext ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        return set_irc_variable(ictx.database, ictx.network, ictx.channel, var, var_text);
    }

    return EvalResult.fail("Variables are not supported in a non-Twitch or non-IRC context.");
}

Result!string get_twitch_variable(Database* db, string channel_name, string variable_name)
{
    try
    {
        auto rows = db.hotzenbot_exec("SELECT variableText FROM TwitchVariables WHERE twitchChannelName = :channel AND variableName = :name;",
                               channel_name, variable_name);
        if (rows.empty)
        {
            return Result!string.fail("No such variable declared in this channel.");
        }
        else
        {
            return Result!string.ok(rows.front.peek!string(0));
        }
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to fetch variable from database: " ~ e.to!string);
        return Result!string.fail("Failed fetching the variable");
    }
}

Result!string get_irc_variable(Database* db, string network_name, string channel_name, string variable_name)
{
    try
    {
        auto rows = db.hotzenbot_exec("SELECT variableText FROM IrcVariables WHERE ircNetworkName = :network AND ircChannelName = :channel AND variableName = :name;",
                               network_name, channel_name, variable_name);
        if (rows.empty)
        {
            return Result!string.fail("No such variable declared in this channel.");
        }
        else
        {
            return Result!string.ok(rows.front.peek!string(0));
        }
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to fetch variable from database: " ~ e.to!string);
        return Result!string.fail("Failed fetching the variable");
    }
}

Result!string set_twitch_variable(Database* db, string channel_name, string variable_name, string variable_text)
{
    try
    {
        db.hotzenbot_exec("INSERT INTO TwitchVariables (variableName, variableText, twitchChannelName) VALUES (:name, :text, :channel);",
                   variable_name, variable_text, channel_name);
        return Result!string.ok("Variable has been set.");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to set variable from database: " ~ e.to!string);
        return Result!string.fail("Setting variable failed. Please consult the logs.");
    }
}

Result!string set_irc_variable(Database* db, string network_name, string channel_name, string variable_name, string variable_text)
{
    try
    {
        db.hotzenbot_exec("INSERT INTO IrcVariables (variableName, variableText, ircNetworkName, ircChannelName) VALUES (:name, :text, :network, :channel);",
                   variable_name, variable_text, network_name, channel_name);
        return Result!string.ok("Variable has been set.");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to set variable from database: " ~ e.to!string);
        return Result!string.fail("Setting variable failed. Please consult the logs.");
    }
}
