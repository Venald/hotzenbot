/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.ffz;

import std.algorithm;
import std.array;
import std.container.dlist;
import std.conv : to;
import std.json;
import std.net.curl;

import hotzenbot.commands.eval.asciify;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import d2sqlite3;

struct FFZEmote
{
    string emote_name;
    string emote_url;
    string emote_channel;
}

struct FFZEmoteSet
{
    FFZEmote[] set_emotes;
}

FFZEmote[] parse_ffz_emote_list(JSONValue emote_list, string channel_name)
{
    return array(emote_list.array.map!(emote =>
        FFZEmote(emote["name"].str,
                 "https:" ~ emote["urls"]
                   .object[emote["urls"]
                           .object.keys
                           .map!(x=>x.to!int)
                           .maxElement
                           .to!string].str,
                 channel_name)));
}

Result!FFZEmoteSet fetch_ffz_emotes_of_twitch_channel(string channel_name)
{
    try
    {
        auto raw_json = get("https://api.frankerfacez.com/v1/room/" ~ channel_name[1..$]);
        auto response = parseJSON(raw_json);

        string set_id = response["room"]["set"].integer.to!string;
        auto channel_emotes = response["sets"][set_id]["emoticons"];
        return Result!FFZEmoteSet.ok(FFZEmoteSet(parse_ffz_emote_list(channel_emotes, channel_name)));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to fetch FFZ emotes for channel " ~ channel_name ~ ": " ~ e.to!string);
        return Result!FFZEmoteSet.fail("Unable to fetch FFZ emotes");
    }
}

Result!string update_ffz_emotes_of_twitch_channel(Database* database, string channel_name)
{
    auto emotes = fetch_ffz_emotes_of_twitch_channel(channel_name);
    if (emotes.is_error)
    {
        return Result!string.fail(emotes.err_msg);
    }

    auto cached_urls = DList!string(database.hotzenbot_exec("SELECT url FROM AsciifyUrlCache;").map!(row => row.peek!string(0)));

    foreach(emote; emotes.result.set_emotes)
    {
        auto asciified = asciify_image_from_url(emote.emote_url);
        if (asciified.is_error)
        {
            return Result!string.fail(asciified.err_msg);
        }

        database.hotzenbot_exec("INSERT INTO AsciifyUrlCache (url, image) VALUES (:url, :image);",
                                emote.emote_url, asciified.result);
        database.hotzenbot_exec("INSERT INTO FfzEmotes (channel, imageUrl, name) VALUES (:channel, :image_url, :name);",
                                channel_name, emote.emote_url, emote.emote_name);
    }

    return Result!string.ok("Successfully updated FFZ emotes of channel " ~ channel_name);
}

Result!string ffz_emote_from_cache(Database* database, string channel, string emote)
{
    try
    {
        auto rows = database.hotzenbot_exec("SELECT channel, imageUrl FROM FfzEmotes WHERE name = :name;", emote);
        foreach(row; rows)
        {
            auto emote_channel = row.peek!string(0);
            auto url = row.peek!string(1);
            if (emote_channel.empty || channel == emote_channel)
            {
                auto asciis = database.hotzenbot_exec("SELECT image FROM AsciifyUrlCache WHERE url = :url;", url);
                if (asciis.empty)
                {
                    return Result!string.fail("Emote cache is invalid. Update the cache with %updateffz");
                }
                else
                {
                    return Result!string.ok(asciis.front.peek!string(0));
                }
            }
        }

        return Result!string.fail("No such ffz emote");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to query emote cache: " ~ e.to!string);
        return Result!string.fail("Cache error");
    }
}
