/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.messagehandler;

import dialect;

import hotzenbot.bot.botversion;
import hotzenbot.bot.markovthread;
import hotzenbot.commands.context;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.pipe;
import hotzenbot.commands.twitchcontext;
import hotzenbot.irc.ircwritethread;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;

import std.conv : to;
import std.datetime.systime;
import std.parallelism;
import std.stdio;
import std.string;

import d2sqlite3.database;

struct IRCNetworkState
{
    struct ChannelState
    {
        string[] nicks;
        string[string] addresses;
        string topic;
    }

    alias channel_states this;

    ChannelState[string] channel_states;
}

class BotMessageHandler
{
    this(Database* database)
    {
        this.database = database;
    }

    Database* database;
    IRCNetworkState[string] irc_network_states;

    private void create_and_run_pipe(Context ctx, string message)
    {
        if (ctx.is_bot_mentioned(message))
        {
            ctx.reply_to_sender(gen_markov_sentence(null));
            return;
        }

        auto maybe_pipe = Pipe.parse(ctx, message);
        if (maybe_pipe.isNull)
        {
            return;
        }
        else
        {
            log_info(LoggingScope.System, "Handling pipe event");
            try
            {
                /* Insert the git hash of the bot into the variable table.
                 * Uses CTFE
                 */
                ctx.variables["version"] = bot_version();

                auto handle_task = task!task_handle_pipe(this, ctx, maybe_pipe.get);
                handle_task.executeInNewThread;
            }
            catch (Exception e)
            {
                log_error(LoggingScope.System, "Unable to spawn pipe handling task: " ~ e.to!string);
            }
        }
    }

    public void on_irc_event(IRCEvent event, string bot_nickname, string network, IRCWriteThread respond_thread)
    {
        log_info(LoggingScope.IRC, "IRC event " ~ event.type.to!string);
        if (network !in irc_network_states)
        {
            irc_network_states[network] = IRCNetworkState();
        }

        if (event.channel !in irc_network_states[network])
        {
            irc_network_states[network][event.channel] = IRCNetworkState.ChannelState();
            // Let's also query the user addresses for the trust system
            respond_thread.push_message("WHO " ~ event.channel ~ "\r\n");
        }

        switch (event.type)
        {
        case IRCEvent.Type.SELFJOIN:
        {
            on_join_irc_channel(event, network);
            return;
        }
        case IRCEvent.Type.SELFPART:
        {
            on_part_irc_channel(event, network);
            return;
        }
        case IRCEvent.Type.TOPIC:
        case IRCEvent.Type.RPL_TOPIC:
        {
            irc_network_states[network][event.channel].topic = event.content;
            log_info(LoggingScope.IRC, "Channel topic of " ~ network ~ "/" ~ event.channel ~ ": " ~ event.content);
            return;
        }
        case IRCEvent.Type.RPL_WHOREPLY:
        {
            irc_network_states[network][event.channel].
                addresses[event.target.nickname]
                = event.target.address;
            return;
        }
        case IRCEvent.Type.JOIN:
        {
            irc_network_states[network][event.channel].
                addresses[event.sender.nickname]
                = event.sender.address;
            return;
        }
        case IRCEvent.Type.PART:
        {
            irc_network_states[network][event.channel].
                addresses.remove(event.sender.nickname);
            return;
        }
        case IRCEvent.Type.RPL_NAMREPLY:
        {
            foreach (user; event.content.split)
            {
                irc_network_states[network][event.channel].nicks ~= user;
            }

            return;
        }
        case IRCEvent.Type.CHAN:
        case IRCEvent.Type.PRIVMSG:
        {
            IRCContext ctx = new IRCContext(this.database,
                                            bot_nickname, network, event,
                                            respond_thread,
                                            &irc_network_states[network][event.channel]);

            log_irc_message(event);

            if (ctx.is_sender_ignored()) {
                log_info(LoggingScope.IRC, "Ignoring message from user " ~ network ~ "/" ~ event.sender.nickname ~ ": " ~ event.content);
                return;
            }

            markov_add_sentence(event.content.dup);

            create_and_run_pipe(ctx, event.content);
            return;
        }
        default:
            break;
        }
    }

    public void on_twitch_event(IRCEvent event, IRCWriteThread respond_thread, string bot_nickname, string token, string client_id)
    {
        log_info(LoggingScope.Twitch, "Twitch event " ~ event.type.to!string);

        switch (event.type)
        {
        case IRCEvent.Type.SELFJOIN:
        {
            on_join_twitch_channel(event);
            return;
        }
        case IRCEvent.Type.SELFPART:
        {
            on_part_twitch_channel(event);
            return;
        }
        case IRCEvent.Type.CHAN:
        case IRCEvent.Type.PRIVMSG:
        {
            log_twitch_message(event);

            TwitchContext ctx = new TwitchContext(database, event, respond_thread, bot_nickname, token, client_id);
            if (ctx.is_sender_ignored()) {
                log_info(LoggingScope.Twitch, "Ignoring message from user " ~ event.sender.displayName ~ ": " ~ event.content);
                return;
            }

            markov_add_sentence(event.content.dup);
            create_and_run_pipe(ctx, event.content);

            return;
        }
        default:
            break;
        }
    }

    private void on_join_twitch_channel(IRCEvent event)
    {
        log_info(LoggingScope.Twitch, "Registering Twitch channel " ~ event.channel );
        database.hotzenbot_exec("INSERT INTO JoinedTwitchChannels " ~
                                "(name) "~
                                "VALUES (:name);",
                                event.channel);
    }

    private void on_join_irc_channel(IRCEvent event, string network)
    {
        log_info(LoggingScope.IRC, "Registering irc channel " ~ network ~ "/" ~ event.channel );
        database.hotzenbot_exec("INSERT INTO JoinedIRCChannels " ~
                                "(name, network) "~
                                "VALUES (:name, :network);",
                                event.channel, network);
    }

    private void on_part_twitch_channel(IRCEvent event)
    {
        log_info(LoggingScope.Twitch, "Unregistering Twitch channel " ~ event.channel );
        database.hotzenbot_exec("DELETE FROM JoinedTwitchChannels " ~
                                "WHERE name = :name;",
                                event.channel);
    }

    private void on_part_irc_channel(IRCEvent event, string network)
    {
        log_info(LoggingScope.IRC, "Unregistering irc channel " ~ network ~ "/" ~ event.channel );
        database.hotzenbot_exec("DELETE FROM JoinedIRCChannels " ~
                                "WHERE name = :name AND network = :network ;",
                                event.channel, network);
    }

    private void log_irc_message(IRCEvent event)
    {
        string senderIrcNickName = event.sender.nickname;
        string senderIrcChannel = event.channel;
        string senderIrcNetwork = event.sender.address;
        string message = event.content;

        try
        {
            database.hotzenbot_exec("INSERT INTO IrcLog " ~
                                    "(senderIrcNickName, senderIrcChannel, senderIrcNetwork, message) "~
                                    "VALUES (:senderIrcNickName, :senderIrcChannel, :senderIrcNetwork, :message);",
                                    senderIrcNickName, senderIrcChannel, senderIrcNetwork, message);
            log_info(LoggingScope.IRC, "Logged message from user " ~ senderIrcNickName ~ ": " ~ message);
        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to log irc message: " ~ e.to!string);
        }
    }

    private void log_twitch_message(IRCEvent event)
    {
        string channel = event.channel;
        string senderTwitchId = event.sender.id.to!string;
        string senderTwitchName = event.sender.nickname;
        string senderTwitchDisplayName = event.sender.displayName;
        string senderTwitchRoles = "";
        // TODO(#18): Logging format for badge roles is not compatible with kgbotka format.
        // Maybe a custom migration is required that updates all the
        // roles. That would break backwards compatibility with
        // kgbotka though.
        string senderTwitchBadgeRoles = event.sender.badges;
        string message = event.content;

        try
        {
            database.hotzenbot_exec("INSERT INTO TwitchLog " ~
                                    "(channel, senderTwitchId, senderTwitchName, senderTwitchDisplayName, senderTwitchRoles, senderTwitchBadgeRoles, message)" ~
                                    "VALUES (:channel, :senderTwitchId, :senderTwitchName, :senderTwitchDisplayName, :senderTwitchRoles, :senderTwitchBadgeRoles, :message);",
                                    channel, senderTwitchId, senderTwitchName, senderTwitchDisplayName, senderTwitchRoles, senderTwitchBadgeRoles, message);

            log_info(LoggingScope.Twitch, "Logged message from user " ~ senderTwitchDisplayName ~ ": " ~ message);
        }
        catch (Exception e)
        {
            log_error(LoggingScope.Twitch, "Unable to log twitch message: " ~ e.to!string);
        }

    }

    static void task_handle_pipe(BotMessageHandler handler,
                                 Context ctx,
                                 Pipe pipe)
    {
        ctx.database.start_transaction();

        try
        {
            ctx.variables["year"] = Clock.currTime().year.to!string;

            auto pipe_result = pipe.run(ctx);
            if (pipe_result.is_error)
                ctx.reply_to_sender(pipe_result.err_msg);
            else
                ctx.reply(pipe_result.result);
            ctx.database.commit_transaction();
        }
        catch (Exception e)
        {
            log_error(LoggingScope.System, "Error in pipe handling task: " ~ e.to!string);
            ctx.database.rollback_transaction();
        }
    }
}
