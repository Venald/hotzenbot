/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.globalsignal;

import std.concurrency;

enum GlobalSignal { Shutdown };
enum GLOBAL_THREAD_NAME = "global";

void shutdown_bot()
{
    auto tid = locate(GLOBAL_THREAD_NAME);
    send(tid, GlobalSignal.Shutdown);
}
