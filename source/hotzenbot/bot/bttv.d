/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.bttv;

import std.algorithm;
import std.container.dlist;
import std.conv : to;
import std.json;
import std.net.curl;
import std.range;

import hotzenbot.commands.eval.asciify;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import d2sqlite3;

struct BTTVEmote
{
    string emote_id;
    string emote_name;
    string channel_name;
}

DList!BTTVEmote query_bttv_emotes_of_twitch_channel(string channel_name)
{
    try
    {
        string raw = cast(string)(get("https://api.betterttv.net/2/channels/" ~ channel_name));
        auto response = parseJSON(raw);

        return DList!BTTVEmote(response["emotes"].array
                               .filter!(emote => emote["imageType"].str == "png")
                               .map!(emote =>
                                     BTTVEmote(emote["id"].str,
                                               emote["code"].str,
                                               channel_name)));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to fetch BTTV emotes of channel " ~
                 channel_name ~ ": " ~ e.to!string);
        return DList!BTTVEmote();
    }
}

string bttv_emote_url(string emote_id)
{
    return "https://cdn.betterttv.net/emote/" ~ emote_id ~ "/3x";
}

Result!string update_bttv_emotes_of_twitch_channel(Database* database, string channel_name)
{
    auto emotes = query_bttv_emotes_of_twitch_channel(channel_name[0] == '#' ? channel_name[1..$] : channel_name);

    auto cached_urls = DList!string(database.hotzenbot_exec("SELECT url FROM AsciifyUrlCache;").map!(row => row.peek!string(0)));

    foreach(emote; emotes)
    {
        auto emote_url = emote.emote_id.bttv_emote_url;
        auto asciified = asciify_image_from_url(emote_url);
        if (asciified.is_error)
        {
            return Result!string.fail(asciified.err_msg);
        }

        database.hotzenbot_exec("INSERT INTO AsciifyUrlCache (url, image) VALUES (:url, :image);",
                                emote_url, asciified.result);
        database.hotzenbot_exec("INSERT INTO BttvEmotes (channel, imageUrl, name) VALUES (:channel, :image_url, :name);",
                                channel_name, emote_url, emote.emote_name);
    }

    return Result!string.ok("Updated BTTV channel emotes for channel " ~ channel_name);
}

Result!string bttv_emote_from_cache(Database* database, string channel, string emote)
{
    try
    {
        auto rows = database.hotzenbot_exec("SELECT channel, imageUrl FROM BttvEmotes WHERE name = :name;", emote);
        foreach(row; rows)
        {
            auto emote_channel = row.peek!string(0);
            auto url = row.peek!string(1);
            if (emote_channel.empty || channel == emote_channel)
            {
                auto asciis = database.hotzenbot_exec("SELECT image FROM AsciifyUrlCache WHERE url = :url;", url);
                if (asciis.empty)
                {
                    return Result!string.fail("Emote cache is invalid. Update the cache with %updatebttv");
                }
                else
                {
                    return Result!string.ok(asciis.front.peek!string(0));
                }
            }
        }

        return Result!string.fail("No such bttv emote");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to query emote cache: " ~ e.to!string);
        return Result!string.fail("Cache error");
    }
}
