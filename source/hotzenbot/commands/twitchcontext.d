/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.twitchcontext;

import hotzenbot.bot.roles;
import hotzenbot.commands.context;
import hotzenbot.irc.ircwritethread;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.algorithm;
import std.conv : to;
import std.container.dlist;
import std.string;

import d2sqlite3;

import dialect;

class TwitchContext : Context
{
    this(Database* database, IRCEvent event, IRCWriteThread respond_thread, string bot_nickname, string token, string client_id)
    {
        this.database = database;
        this.channel = event.channel;
        this.userTwitchId = event.sender.id;
        this.write_thread = respond_thread;
        this.sender_badge_roles = badge_roles_of_twitch_event(event);
        this.access_token = token;
        this.client_id = client_id;
        this.bot_nickname = bot_nickname;
        this.emotes = event.emotes;

        // Prepopulate twitch-specific variables
        this.variables["tchannel"] = this.channel[1..$];
        this.variables["sender"] = event.sender.displayName;
        this.sender = event.sender.displayName;
    }

    string channel;
    string server;
    string sender;
    string access_token;
    string client_id;
    string bot_nickname;
    string emotes;
    int userTwitchId;
    IRCWriteThread write_thread;
    DList!Role sender_badge_roles;

    override void reply(string message)
    {
        while (message.startsWith('/') || message.startsWith('.'))
        {
            message = message[1..$];
        }

        /*
         * WARNING: Ugly hack, see #49.
         */
        if (message.length > 1200)
        {
            log_warn(LoggingScope.Twitch, "Trimming message to 1200 characters");
            message = message[0..1197] ~ "\r\n";
        }

        write_thread.push_message("PRIVMSG " ~ channel ~ " :" ~ message ~ "\r\n");
    }

    override void reply_to_sender(string message)
    {
        reply("@" ~ sender ~ ", " ~ message);
    }

    override char call_prefix()
    {
        auto settings = database.hotzenbot_exec("SELECT channelCommandPrefix FROM JoinedTwitchChannels WHERE name = :channel", channel);
        if (settings.empty)
        {
            // TODO(#20): If no call prefix is set, the one from the Settings should be used instead
            return '!';
        }

        return settings.front.peek!string(0)[0];
    }

    override void log_command_call(int command_id, string args)
    {
        try
        {
            auto statement = this.database.hotzenbot_exec("INSERT INTO CommandLog (userTwitchId, commandId, commandArgs) " ~
                                                          "VALUES (:twitchid, :cmdid, :args);",
                                                          userTwitchId.to!string, command_id, !args ? "" : args);

            log_debug(LoggingScope.Twitch, "Logged command invocation");
        }
        catch (Exception e)
        {
            log_error(LoggingScope.Twitch, e.to!string);
        }
    }

    override bool is_sender_trusted()
    {
        auto roles = database.custom_roles_of_twitch_user(userTwitchId);
        return roles[].any!"a.is_trusted"() || sender_badge_roles[].any!"a.is_trusted"();
    }

    override bool is_sender_maintainer()
    {
        auto roles = database.custom_roles_of_twitch_user(userTwitchId);
        return roles[].any!"a.is_maintainer" || sender_badge_roles[].any!"a.is_maintainer"();
    }

    override bool is_sender_authority()
    {
        auto roles = database.custom_roles_of_twitch_user(userTwitchId);
        return roles[].any!"a.is_authority" || sender_badge_roles[].any!"a.is_authority";
    }


    override bool is_bot_mentioned(string message)
    {
        return message.startsWith("@" ~ this.bot_nickname) || message.startsWith(this.bot_nickname);
    }

    override bool is_sender_ignored()
    {
        auto roles = database.custom_roles_of_twitch_user(userTwitchId);
        return roles[].any!"a.is_ignored";
    }

    override Result!double get_calc_var(string var_name)
    {
        try
        {
            auto results = database.hotzenbot_exec(
                "SELECT * FROM CalcVariables WHERE calcVarTwitchId = :id " ~
                "AND calcVarName = :varName;",
                userTwitchId, var_name);

            if (results.empty)
            {
                return Result!double.fail("Variable `" ~ var_name ~ "` is undefined.");
            }
            else
            {
                return Result!double.ok(results.front["calcVarValue"].as!double);
            }
        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to fetch variable `" ~ var_name ~ "`: " ~ e.to!string);
            return Result!double.fail("Unable to fetch variable `" ~ var_name ~ "`.");
        }
    }

    override Result!string set_calc_var(string var_name, double value)
    {
        try
        {
            database.hotzenbot_exec(
                "INSERT INTO CalcVariables (calcVarTwitchId, calcVarName, calcVarValue) " ~
                "VALUES (:id, :varName, :value);",
                userTwitchId, var_name, value);
            return Result!string.ok("Variable has been set.");
        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to set variable `" ~ var_name ~ "`: " ~ e.to!string);
            return Result!string.fail("Unable to set variable `" ~ var_name ~ "`.");
        }
    }

    override Result!string timeout_sender(uint seconds)
    {
        write_thread.push_message("PRIVMSG " ~ channel ~ " :" ~
                                  "/timeout " ~ sender ~ " " ~ seconds.to!string ~ "\r\n");
        return Result!string.ok("Timed out user.");
    }
}
