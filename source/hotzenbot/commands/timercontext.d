module hotzenbot.commands.timercontext;

import hotzenbot.commands.context;
import hotzenbot.sys.result;

import d2sqlite3;

class TimerContext : Context
{
    this(Database* database)
    {
        this.database = database;
    }

    override char call_prefix()
    {
        return '!';
    }

    override void log_command_call(int _, string __)
    {
    }

    override void reply_to_sender(string _)
    {
        throw new Exception("Tried to reply from the timer context");
    }

    override void reply(string _)
    {
        throw new Exception("Tried to reply from the timer context");
    }

    override bool is_bot_mentioned(string _)
    {
        return false;
    }

    override bool is_sender_trusted()
    {
        return true;
    }

    override bool is_sender_authority()
    {
        return false;
    }

    override bool is_sender_maintainer()
    {
        return false;
    }

    override bool is_sender_ignored()
    {
        return false;
    }

    override Result!double get_calc_var(string var_name)
    {
        return Result!double.fail("Cannot fetch calc variables in the timer context");
    }

    override Result!string set_calc_var(string var_name, double val)
    {
        return Result!string.fail("Calc variables are unavailable in the timer context.");
    }

    override Result!string timeout_sender(uint seconds)
    {
        return Result!string.fail("Cannot timeout in the timer context");
    }
}
