/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.expression;

import std.algorithm;
import std.array;
import std.container.dlist;
import std.conv;
import std.range;
import std.regex;
import std.string;
import std.traits;

import hotzenbot.bot.roles;
import hotzenbot.bot.globalsignal;
import hotzenbot.bot.variables;
import hotzenbot.commands.context;
import hotzenbot.commands.eval.asciify;
import hotzenbot.commands.eval.authority;
import hotzenbot.commands.eval.calc;
import hotzenbot.commands.eval.faker;
import hotzenbot.commands.eval.markov;
import hotzenbot.commands.eval.misc;
import hotzenbot.commands.eval.network;
import hotzenbot.commands.eval.or;
import hotzenbot.commands.eval.quotes;
import hotzenbot.commands.eval.russify;
import hotzenbot.commands.eval.timer;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

interface Expression
{
    EvalResult evaluate(Context);
    string to_string();
}

final class ListExpression : Expression
{
    DList!Expression expressions;

    EvalResult evaluate(Context context)
    {
        string list;
        foreach (expr; expressions)
        {
            auto res = expr.evaluate(context);
            if (res.is_error)
            {
                return res;
            }
            else
            {
                list ~= res.result;
            }
        }

        return EvalResult.ok(list);
    }

    string to_string()
    {
        string result;
        foreach(expr; expressions)
        {
            result ~= expr.to_string;
        }

        return result;
    }
}

final class VariableExpression : Expression
{
    string name;

    this(string name)
    {
        this.name = name;
    }

    EvalResult evaluate(Context context)
    {
        if (name in context.variables)
        {
            return EvalResult.ok(context.variables[name]);
        }
        else
        {
            return EvalResult.fail("Variable `" ~ name ~ "` is undefined");
        }
    }

    string to_string()
    {
        return "%" ~ name;
    }
}

final class TextExpression : Expression
{
    string content;

    EvalResult evaluate(Context context)
    {
        return EvalResult.ok(content);
    }

    this(string content)
    {
        this.content = content;
    }

    string to_string()
    {
        return "\"" ~ content ~ "\"";
    }
}

final class FunCallExpression : Expression
{
    string function_name;
    DList!Expression args;

    EvalResult evaluate(Context context)
    {
        switch (function_name)
        {
        static foreach (file; ___files)
        {
            mixin(fake_cases!("fakedata/" ~ file)());
        }
        case "asciify": {
            return run_with_context(context, args, &asciify);
        }
        case "updatebttv": {
            return run_with_context(context, args, &update_bttv);
        }
        case "updateffz": {
            return run_with_context(context, args, &update_ffz);
        }
        case "russify": {
            return run_without_context(context, args, &russify);
        }
        case "derussify": {
            return run_without_context(context, args, &derussify);
        }
        case "or": {
            return or(context, args);
        }
        case "urlencode": {
            return run_without_context(context, args, &urlencode);
        }
        case "curl": {
            return run_without_context(context, args, &curl);
        }
        case "calc": {
            return run_with_context(context, args, &calc);
        }
        case "a_an": {
            return run_without_context(context, args, &a_an);
        }
        case "get_var": {
            return run_with_context(context, args, &get_var);
        }
        case "set_var": {
            auto var_name = args.front.evaluate(context);
            if (var_name.is_error)
            {
                return var_name;
            }

            string var_text;
            foreach(arg; args[].dropOne)
            {
                auto tmp = arg.evaluate(context);
                if (tmp.is_error)
                {
                    return tmp;
                }
                var_text ~= tmp.result;
            }

            return set_var(context, var_name.result, var_text);
        }
        case "set_timer": {
            auto duration = args.front.evaluate(context);
            if (duration.is_error)
            {
                return duration;
            }

            string message;
            foreach(arg; args[].dropOne)
            {
                auto tmp = arg.evaluate(context);
                if (tmp.is_error)
                {
                    return tmp;
                }
                message ~= tmp.result;
            }

            return set_timer(context, message, duration.result);
        }
        case "markov": {
            return run_with_context(context, args, &markov);
        }
        case "retrain_markov": {
            return run_without_context(context, args, &retrain_markov);
        }
        case "add_quote": {
            return run_with_context(context, args, &add_quote);
        }
        case "get_quote": {
            return get_quote(context);
        }
        case "trusted": {
            if (context.is_sender_trusted)
            {
                return evaluate_concat_all(context, args);
            }
            else
            {
                return EvalResult.fail("Only for trusted users");
            }
        }
        case "authority": {
            if (context.is_sender_authority)
            {
                return evaluate_concat_all(context, args);
            }
            else
            {
                return EvalResult.fail("Only for broadcaster and mods.");
            }
        }
        case "maintainer": {
            if (context.is_sender_maintainer)
            {
                return evaluate_concat_all(context, args);
            }
            else
            {
                return EvalResult.fail("Only for bot maintainers.");
            }
        }
        case "assrole": {
            if (context.is_sender_maintainer)
            {
                auto _args = array(args);
                if (_args.length != 2)
                {
                    return EvalResult.fail("Need a user and a role-name");
                }

                auto user_name = _args[0].evaluate(context);
                if (user_name.is_error)
                {
                    return user_name;
                }

                auto role_name = _args[1].evaluate(context);
                if (role_name.is_error)
                {
                    return role_name;
                }

                return context.ass_role(user_name.result, role_name.result);
            }
            return EvalResult.fail("Only for maintainers");
        }
        case "addrole": {
            if (!context.is_sender_maintainer)
            {
                return EvalResult.fail("Only for maintainers");
            }

            auto role_name = evaluate_concat_all(context, args);
            if (role_name.is_error)
            {
                return EvalResult.fail(role_name.err_msg);
            }

            auto add_role_result = context.database.add_twitch_role(role_name.result);
            if (add_role_result.is_error)
            {
                return EvalResult.fail(add_role_result.err_msg);
            }
            else
            {
                return EvalResult.ok(add_role_result.result);
            }
        }
        case "roles": {
            auto ctx = cast(TwitchContext)context;
            if (ctx is null)
            {
                return EvalResult.fail("%roles works only on Twitch");
            }

            auto roles = ctx.database.custom_roles_of_twitch_user(ctx.userTwitchId);
            string result;
            foreach (role; roles)
            {
                if (result)
                    result ~= ", " ~ role.name;
                else
                    result = role.name;
            }

            foreach (role; ctx.sender_badge_roles)
            {
                if (result)
                    result ~= ", " ~ role.name;
                else
                    result = role.name;
            }

            return EvalResult.ok(result);
        }
        case "timeout_sender": {
            if (args.array.length != 1) {
                return EvalResult.fail("timeout_sender expects 1 argument");
            }

            auto maybe_time = args.front.evaluate(context);
            if (maybe_time.is_error) {
                return maybe_time;
            }

            try
            {
                auto num_of_secs = maybe_time.result.parse!uint;
                context.timeout_sender(num_of_secs);
                return EvalResult.ok("");
            }
            catch (Exception e)
            {
                return EvalResult.fail("Expected number of seconds for timeout");
            }
        }
        case "emotify": {
            auto _args = args.array;
            if (_args.length != 3) {
                return EvalResult.fail("emotify expects 3 argument");
            }

            auto match = _args[0].evaluate(context);
            if (match.is_error) {
                return match;
            }

            auto replace = _args[1].evaluate(context);
            if (replace.is_error) {
                return replace;
            }

            auto in_sentence = evaluate_concat_all(context, _args[2..$]);
            if (in_sentence.is_error) {
                return in_sentence;
            }

            return emotify(context, match.result[0], replace.result, in_sentence.result);
        }
        case "eval": {
            if (context.is_sender_maintainer)
            {
                return run_with_context(context, args, &eval);
            }
            else
            {
                return EvalResult.fail("The %eval-function is only available for bot maintainers.");
            }
        }
        case "local_time": {
            return run_with_context(context, args, &local_time);
        }
        case "void": {
            foreach (arg; args)
            {
                auto res = arg.evaluate(context);
                if (res.is_error)
                {
                    return res;
                }
            }

            return EvalResult.ok("");
        }
        case "shutdown": {
            if (context.is_sender_maintainer)
            {
                shutdown_bot();
                return EvalResult.ok("Shutting down the bot.");
            }
            else
            {
                return EvalResult.fail("The %shutdown-function is only available for bot maintainers.");
            }
        }
        case "cat": {
            return evaluate_concat_all(context, args);
        }
        case "getopt": {
            auto _args = args.array;
            if (_args.length < 4)
            {
                return EvalResult.fail("getopt expects at least 4 arguments");
            }

            auto pattern = _args[0].evaluate(context);
            if (pattern.is_error)
            {
                return pattern;
            }

            auto var_names = _args[1].evaluate(context);
            if (var_names.is_error)
            {
                return var_names;
            }

            auto expression = _args[2].evaluate(context);
            if (expression.is_error)
            {
                return expression;
            }

            try
            {
                auto compiled_regex = regex(pattern.result);
                auto captures = expression.result.matchFirst(compiled_regex);
                foreach (capture; var_names.result.split(',').zip(captures.dropOne))
                {
                    context.variables[capture[0]] = capture[1];
                }

                return evaluate_concat_all(context, _args[3..$]);
            }
            catch (Exception e)
            {
                log_error(LoggingScope.System, "Failed to compile getopt-regex: " ~ e.to!string);
                return EvalResult.fail("Failed to compile getopt-regex");
            }
        }
        default:
            return EvalResult.fail(function_name ~ ": No such eval function.");
        }
    }

    string to_string()
    {
        string result = "%" ~ function_name ~ "(";

        foreach(arg; args)
        {
            result ~= arg.to_string ~ ", ";
        }

        return result ~ ")";
    }
}

EvalResult evaluate_concat_all(T)(Context ctx, T exprs)
if (isIterable!T)
{
    EvalResult result = EvalResult.ok(null);

    foreach(expr; exprs)
    {
        auto curr_result = expr.evaluate(ctx);
        if (curr_result.is_error)
        {
            return curr_result;
        }

        if (result.result)
            result.result ~= curr_result.result;
        else
            result = curr_result;
    }

    return result;
}

EvalResult run_without_context(T)(Context ctx, T args, EvalResult function(string) f)
if (isIterable!T)
{
    auto args_result = evaluate_concat_all(ctx, args);
    if (args_result.is_error)
    {
        return args_result;
    }
    else
    {
        return f(args_result.result);
    }
}

EvalResult run_with_context(T)(Context ctx, T args, EvalResult function(Context, string) f)
if (isIterable!T)
{
    auto args_result = evaluate_concat_all(ctx, args);
    if (args_result.is_error)
    {
        return args_result;
    }
    else
    {
        return f(ctx, args_result.result);
    }
}
