/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.misc;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime;
import std.format;
import std.random;
import std.range;
import std.uni;

import hotzenbot.commands.context;
import hotzenbot.commands.expressionparser;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

EvalResult a_an(string input)
{
    if (input.empty)
    {
        return EvalResult.ok(input);
    }

    if ("aeiou".canFind(input[0].toLower))
    {
        return EvalResult.ok("an " ~ input);
    }
    else
    {
        return EvalResult.ok("a " ~ input);
    }
}

EvalResult eval(Context ctx, string input_expr)
{
    auto parser = ExpressionParser(input_expr);
    auto parsed_expr = parser.parse();
    if (parsed_expr.is_error) {
        return EvalResult.fail(parsed_expr.err_msg);
    }

    return parsed_expr.result.evaluate(ctx);
}

EvalResult emotify(Context ctx, char character, string replacement, string input)
{
    try
    {
        ulong[] indicies = input
            .map!toUpper
            .enumerate
            .filter!(x => x[1] == character.toUpper)
            .map!(x => x[0])
            .array
            .sort!((x, y) => x > y)
            .array;

        if (indicies.length > 3)
        {
            indicies = indicies
                .randomSample(3)
                .array
                .sort!((x, y) => x > y)
                .array;
        }

        foreach (index; indicies)
        {
            input.replaceInPlace(index, index+1, " " ~ replacement ~ " ");
        }
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Failed to emotify `" ~ input ~ "`: " ~ e.to!string);
    }

    return EvalResult.ok(input);
}

EvalResult local_time(Context ctx, string args)
{
    if (args.canFind("..") || args.canFind("~") || args.startsWith("/"))
    {
        return EvalResult.fail("path traversal vulnerabilities in " ~ Clock.currTime.year.to!string);
    }

    try
    {
        auto tz = PosixTimeZone.getTimeZone(args);
        auto time = Clock.currTime().toOtherTZ(tz);
        return EvalResult.ok(`%02d:%02d`.format(time.hour, time.minute));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to parse time zone " ~ args ~ ": " ~ e.to!string);
        return EvalResult.fail("Unable to parse time zone " ~ args);
    }
}
