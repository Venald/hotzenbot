/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.markov;

import hotzenbot.bot.markovthread;
import hotzenbot.commands.context;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.string;
import std.exception;
import std.conv;
import std.range;

import d2sqlite3;

EvalResult markov(Context ctx, string seed)
{
    try
    {
        return EvalResult.ok(gen_markov_sentence(seed));
    }
    catch (Exception e)
    {
        log_fatal(LoggingScope.Markov, "Unable to generate markov sentence: " ~ e.to!string);
        return EvalResult.fail("Unable to generate markov sentence.");
    }
}

EvalResult retrain_markov(string _)
{
    request_retrain_markov();
    return EvalResult.ok("Markov retraining has been started. Refer to the log for more details.");
}
