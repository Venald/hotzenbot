/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.russify;

import std.conv : to;

import hotzenbot.sys.result;

/*
 * WARNING: UGLY CODE
 */

EvalResult derussify(string message)
{
    string[dchar] demazarusha =
        [ 'а': "a",
          'б': "b",
          'в': "v",
          'г': "g",
          'д': "d",
          'е': "ye",
          'ё': "yo",
          'ж': "j",
          'з': "z",
          'и': "ee",
          'й': "y",
          'к': "k",
          'л': "l",
          'м': "m",
          'н': "n",
          'о': "o",
          'п': "p",
          'р': "r",
          'с': "s",
          'т': "t",
          'у': "oo",
          'ф': "f",
          'х': "kh",
          'ц': "ts",
          'ч': "ch",
          'ш': "sh",
          'щ': "sch",
          'ъ': "",
          'ы': "y",
          'ь': "",
          'э': "eh",
          'ю': "yu",
          'я': "ya",
          'А': "A",
          'Б': "B",
          'В': "V",
          'Г': "G",
          'Д': "D",
          'Е': "Ye",
          'Ё': "Yo",
          'Ж': "J",
          'З': "Z",
          'И': "Ee",
          'Й': "Y",
          'К': "K",
          'Л': "L",
          'М': "M",
          'Н': "N",
          'О': "O",
          'П': "P",
          'Р': "R",
          'С': "S",
          'Т': "T",
          'У': "Oo",
          'Ф': "F",
          'Х': "Kh",
          'Ц': "Ts",
          'Ч': "Ch",
          'Ш': "Sh",
          'Щ': "Sch",
          'Ъ': "",
          'Ы': "Y",
          'Ь': "",
          'Э': "Eh",
          'Ю': "Yu",
          'Я': "Ya"];

    string result;

    foreach (c; message.to!dstring)
    {
        if (c in demazarusha)
        {
            result ~= demazarusha[c];
        }
        else
        {
            result ~= c;
        }
    }

    return EvalResult.ok(result);
}

EvalResult russify(string message)
{
    dchar[dchar] mazarusha =
        [ 'a': 'д',
          'e': 'ё',
          'b': 'б',
          'h': 'н',
          'k': 'к',
          'm': 'м',
          'n': 'и',
          'o': 'ф',
          'r': 'г',
          't': 'т',
          'u': 'ц',
          'x': 'ж',
          'w': 'ш',
          'A': 'Д',
          'G': 'Б',
          'E': 'Ё',
          'N': 'И',
          'O': 'Ф',
          'R': 'Я',
          'U': 'Ц',
          'W': 'Ш',
          'X': 'Ж',
          'Y': 'У'];

    string result;

    foreach (c; message)
    {
        if (c in mazarusha)
        {
            result ~= mazarusha[c];
        }
        else
        {
            result ~= c;
        }
    }

    return EvalResult.ok(result);
}
