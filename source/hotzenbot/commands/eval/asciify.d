/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.asciify;

import imageformats;

import d2sqlite3;

import std.algorithm;
import std.array;
import std.conv : to;
import std.net.curl;
import std.range : chunks;

import hotzenbot.bot.bttv;
import hotzenbot.bot.ffz;
import hotzenbot.commands.context;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

// The library reading images is so broken, that I have to manually
// convert the images to greyscale...now at least the alpha channel
// works!
IFImage read_as_y(ubyte[] buffer)
{
    auto src_img = read_image_from_mem(buffer, ColFmt.YA);

    ubyte[] dest_buf;

    for (uint row = 0; row < src_img.h; ++row)
    {
        for (uint col = 0; col < src_img.w; ++col)
        {
            uint base_idx = 2 * (row * src_img.w + col);
            double y = cast(double)(src_img.pixels[base_idx + 0]);
            double a = (cast(double)(src_img.pixels[base_idx + 1])) / 255.0;
            dest_buf ~= cast(ubyte)(y * a);
        }
    }

    return IFImage(src_img.w, src_img.h, ColFmt.Y, dest_buf.dup);
}

ubyte[] resize_image_width(IFImage image, uint dest_width)
{
    uint dest_height = cast(uint)((cast(float)dest_width / cast(float)(image.w)) * cast(float)image.h);
    ubyte[] dest_buffer;

    float scale_factor = cast(float)(image.w) / cast(float)(dest_width);

    for (uint row = 0; row < dest_height; ++row)
    {
        uint row_src = cast(uint)(scale_factor * cast(float)(row));
        for (uint col = 0; col < dest_width; ++col)
        {
            uint col_src = cast(uint)(scale_factor * cast(float)(col));
            // TODO(#50): Average the values in a rectangle
            uint idx_src = row_src * image.w + col_src;
            dest_buffer ~= image.pixels[idx_src];
        }
    }

    return dest_buffer;
}

ubyte avg_brightness(ubyte[] data)
{
    immutable auto len = cast(float)(data.length);
    float avg = 0.0;
    foreach (pxl; data)
    {
        avg += cast(float)(pxl) / len;
    }

    return cast(ubyte)(avg);
}

struct ImageChunk
{
    ubyte[] chunk_data;
}

const uint CHUNK_HEIGHT = 4,
    CHUNK_WIDTH = 2;

ImageChunk[] chunkify_image(ubyte[] image_data, uint width)
{
    uint height = cast(uint)(image_data.length) / width;
    uint max_x_chunks = width / CHUNK_WIDTH;
    uint max_y_chunks = height / CHUNK_HEIGHT;

    ImageChunk[] result;

    for (uint y = 0; y < max_y_chunks; ++y)
    {
        for (uint x = 0; x < max_x_chunks; ++x)
        {
            ImageChunk local_chunk;
            for (uint y_local = 0; y_local < CHUNK_HEIGHT; ++y_local)
            {
                auto y_eff = y * CHUNK_HEIGHT + y_local;
                auto idx = y_eff * width + (x * CHUNK_WIDTH);
                local_chunk.chunk_data ~= image_data[idx];
                local_chunk.chunk_data ~= image_data[idx + 1];
            }

            result ~= local_chunk;
        }
    }

    return result;
}

dchar asciify_chunk(ImageChunk chunk, ubyte threshold)
{
    dchar result = '\u2800';
    const uint[] dot_indicies = [0, 3, 1, 4, 2, 5, 6, 7];
    for (uint y = 0; y < CHUNK_HEIGHT; ++y)
    {
        for (uint x = 0; x < CHUNK_WIDTH; ++x)
        {
            uint pixel_idx = y * CHUNK_WIDTH + x;
            if (chunk.chunk_data[pixel_idx] >= threshold)
            {
                result |= (0x01 << dot_indicies[pixel_idx]);
            }
        }
    }

    return result;
}

uint calculate_new_width(IFImage image)
{
    if (image.w > image.h)
    {
        return 50;
    }
    else
    {
        return cast(uint)((50.0 / cast(double)(image.h)) * cast(double)(image.w));
    }
}


// TODO(#51): asciify_image_from_url does not support GIF images
Result!dstring asciify_image_from_url(string url)
{
    try
    {
        ubyte[] buffer = get!(HTTP, ubyte)(url);

        auto image = read_as_y(buffer);
        auto new_width = calculate_new_width(image);
        auto resized = resize_image_width(image, new_width);
        auto threshold = avg_brightness(resized);
        auto image_chunks = chunkify_image(resized, new_width);
        return Result!dstring.ok(
            cast(dstring)(array(
                                image_chunks
                                .map!(x => asciify_chunk(x, threshold))
                                .chunks(new_width / CHUNK_WIDTH)
                                .join("    "))));
    }
    catch (Exception e)
    {
        return Result!dstring.fail("Failed to asciify image: " ~ e.to!string);
    }
}

EvalResult get_first_twitch_emote(string emotes)
{
    if (emotes.empty)
    {
        return EvalResult.fail("No emotes provided");
    }

    auto emote_list = emotes.split("/");
    if (emote_list.empty)
    {
        return EvalResult.fail("No emotes in list");
    }

    auto emote = emote_list[0].split(":")[0];
    return EvalResult.ok(emote);
}

string twitch_emote_url(string emote)
{
    return "https://static-cdn.jtvnw.net/emoticons/v1/" ~ emote ~ "/3.0";
}

EvalResult asciify_twitch_emote(Database* database, string emotes)
{
    auto maybe_emote = get_first_twitch_emote(emotes);
    if (maybe_emote.is_error)
    {
        return maybe_emote;
    }

    auto emote_url = twitch_emote_url(maybe_emote.result);

    try
    {
        auto actual_emotes = database.hotzenbot_exec("SELECT image FROM AsciifyUrlCache WHERE url = :url;", emote_url);

        if (actual_emotes.empty)
        {
            // Cache miss
            auto emote_to_cache = asciify_image_from_url(emote_url);
            if (emote_to_cache.is_error)
            {
                return EvalResult.fail(emote_to_cache.err_msg);
            }

            database.hotzenbot_exec("INSERT INTO AsciifyUrlCache (image, url) VALUES (:image, :url);",
                                    emote_to_cache.result, emote_url);
            return EvalResult.ok(cast(string)(emote_to_cache.result));
        }

        return EvalResult.ok(actual_emotes.front["image"].as!string);

    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to fetch emotes from database: " ~ e.to!string);
        return EvalResult.fail("Unable to fetch emotes from database");
    }
}

EvalResult asciify(Context _ctx, string emote)
{
    TwitchContext ctx = cast(TwitchContext)_ctx;
    if (ctx is null)
    {
        return EvalResult.fail("Cannot asciify in a non-twitch context.");
    }

    try
    {
        auto twitch = asciify_twitch_emote(ctx.database, ctx.emotes);
        if (!twitch.is_error)
        {
            return twitch;
        }

        auto bttv = bttv_emote_from_cache(ctx.database, ctx.channel, emote);
        if (!bttv.is_error)
        {
            return bttv;
        }

        auto ffz = ffz_emote_from_cache(ctx.database, ctx.channel, emote);
        if (!ffz.is_error)
        {
            return ffz;
        }

        return EvalResult.fail("No such emote");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Failed to asciify emote: " ~ e.to!string);
        return EvalResult.fail("Unable to asciify emote.");
    }
}

EvalResult update_bttv(Context _ctx, string msg)
{
    TwitchContext ctx = cast(TwitchContext)_ctx;
    if (ctx is null)
    {
        return EvalResult.fail("Cannot asciify in a non-twitch context.");
    }

    try
    {
        return update_bttv_emotes_of_twitch_channel(ctx.database, ctx.channel);
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "BTTV cache update failed: " ~ e.to!string);
        return EvalResult.fail("Failed to update BTTV emote cache.");
    }
}

EvalResult update_ffz(Context _ctx, string msg)
{
    TwitchContext ctx = cast(TwitchContext)_ctx;
    if (ctx is null)
    {
        return EvalResult.fail("Cannot asciify in a non-twitch context.");
    }

    try
    {
        return update_ffz_emotes_of_twitch_channel(ctx.database, ctx.channel);
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "FFZ cache update failed: " ~ e.to!string);
        return EvalResult.fail("Failed to update FFZ emote cache.");
    }
}

unittest
{
    auto image = "https://cdn.frankerfacez.com/c539608c9d6f4a549675a45f0e1d6199.png".get!(HTTP,ubyte).read_image_from_mem(ColFmt.YA);

    assert(image.pixels.length == 2 * image.w * image.h);
}
