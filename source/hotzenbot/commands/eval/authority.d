/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.authority;

import std.conv : to;
import std.json;
import std.net.curl;
import std.range;

import hotzenbot.bot.roles;
import hotzenbot.commands.context;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.replcontext;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

EvalResult ass_role(Context ctx, string user_name, string role_name)
{
    auto tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        return assign_role_to_twitch_user(ctx, user_name, role_name, tctx.access_token, tctx.client_id);
    }

    auto rctx = cast(ReplContext)(ctx);
    if (rctx !is null)
    {
        return assign_role_to_twitch_user(ctx, user_name, role_name, rctx.access_token, rctx.client_id);
    }

    auto ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        if (user_name in ictx.channel_state.addresses)
        {
            return assign_role_to_irc_user(ctx, user_name, role_name, ictx.network, ictx.channel_state.addresses[user_name]);
        }
        else
        {
            return EvalResult.fail("User " ~ user_name ~ " is not joined to this channel.");
        }
    }

    return EvalResult.fail("Unknown context type");
}

EvalResult assign_role_to_irc_user(Context ctx, string user_nick, string role_name, string network, string user_address)
{
    try
    {
        auto _role_id = ctx.database.get_twitch_role_id_by_name(role_name);
        if (_role_id.is_error)
        {
            return EvalResult.fail(_role_id.err_msg);
        }

        int role_id = _role_id.result;

        ctx.database.hotzenbot_exec("INSERT INTO IrcUserRoles (userNetwork, userNickname, userAddress, roleId) " ~
                                    "VALUES (:user_network, :user_nick, :user_addr, :role_id);",
                                    network, user_nick, user_address, role_id);
        log_info(LoggingScope.IRC, "Assigned role `" ~ role_name ~ "` to user " ~ user_nick);
        return EvalResult.ok("Assigned role `" ~ role_name ~ "` to user " ~ user_nick ~ ".");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.IRC, "Failed to assign user role: " ~ e.to!string);
        return EvalResult.fail("Unable to assign user role. Check the logs.");
    }
}

EvalResult assign_role_to_twitch_user(Context ctx, string user_name, string role_name, string access_token, string client_id)
{
    try
    {
        /*
         * Find the user id
         */
        auto client = HTTP();
        client.addRequestHeader("Authorization", "Bearer " ~ access_token);
        client.addRequestHeader("Client-ID", client_id);
        string user_id_response = cast(string)(get("https://api.twitch.tv/helix/users?login=" ~ user_name, client));

        auto parsed_response = parseJSON(user_id_response);
        if ("data" !in parsed_response.object)
        {
            log_error(LoggingScope.Twitch, "No data field in Twitch API reponse");
            return EvalResult.fail("Unable to assign role. Please check the logs");
        }

        if (parsed_response.object["data"].array.empty)
        {
            return EvalResult.fail("No such user.");
        }

        int user_id = parsed_response.object["data"].array[0].object["id"].str.to!int;

        /*
         * Find the role id
         */
        auto _role_id = ctx.database.get_twitch_role_id_by_name(role_name);
        if (_role_id.is_error)
        {
            return EvalResult.fail(_role_id.err_msg);
        }

        int role_id = _role_id.result;

        /*
         * Assign the role
         */
        ctx.database.hotzenbot_exec("INSERT INTO TwitchUserRoles (userId, roleId) VALUES (:user_id, :role_id);", user_id, role_id);
        log_info(LoggingScope.Twitch, "Assigned role `" ~ role_name ~ "` to " ~ user_name ~ ".");
        return EvalResult.ok("Assigned role `" ~ role_name ~ "` to " ~ user_name ~ ".");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to assign role: " ~ e.to!string);
        return EvalResult.fail("Unable to assign role. Please check the logs");
    }
}
