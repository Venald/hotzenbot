/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.or;

import hotzenbot.commands.expression;
import hotzenbot.commands.context;
import hotzenbot.sys.result;

import std.string;

EvalResult or(Expressions)(Context ctx, Expressions exprs)
{
    foreach (expr; exprs)
    {
        auto eval_result = expr.evaluate(ctx);

        if (eval_result.is_error)
        {
            continue;
        }

        if (!eval_result.result || eval_result.result.empty)
        {
            continue;
        }

        return eval_result;
    }

    return EvalResult.ok(null);
}
