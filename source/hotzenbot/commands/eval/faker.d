/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.faker;

import hotzenbot.sys.result;

import std.algorithm;
import std.array;
import std.stdio;
import std.json;
import std.file;
import std.random;

static string generate_fake_functions(string path)()
{
    auto raw_json = import(path);
    auto data = parseJSON(raw_json);

    string result;

    foreach (key; data.object.keys)
    {
        string list = data[key].array.map!(x => "\"" ~ x.str ~ "\"").join(", ");
        string d_code = "const string[] data_" ~ key ~ " = [ " ~ list ~ " ];\n" ~
            "EvalResult " ~ key ~ "()\n" ~
            "{\n" ~
            "    return EvalResult.ok(data_" ~ key ~ ".randomSample(1).front);\n" ~
            "}\n";
        result ~= d_code;
    }

    return result;
}

const string[] ___files = ["space.json", "zelda.json", "hacker.json", "random_stuff.json", "bullshit.json"];

static foreach (file; ___files)
{
    mixin(generate_fake_functions!("fakedata/" ~ file)());
}

static string fake_cases(string path)()
{
    auto raw_json = import(path);
    auto data = parseJSON(raw_json);

    string result;

    foreach (key; data.object.keys)
    {
        result ~= "case \"" ~ key ~ "\": return " ~ key ~ "();\n";
    }

    return result;
}
