/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.expressionparser;

import hotzenbot.commands.expression;
import hotzenbot.sys.result;

import std.algorithm;
import std.ascii;
import std.container.dlist;
import std.string;

alias ParserResult = Result!Expression;

struct ExpressionParser
{
    this(string input)
    {
        this.input = input;
    }

    string input;

    ParserResult parse()
    {
        auto result = new ListExpression();

        while (!input.strip.empty)
        {
            auto curr_result = next_expression();
            if (curr_result.is_error)
            {
                return curr_result;
            }

            result.expressions.insert(curr_result.result);
        }

        return ParserResult.ok(result);
    }

    ParserResult next_expression()
    {
        if (input.length == 0)
        {
            return ParserResult.fail("Empty expression");
        }

        for (int i = 0; i < input.length; ++i)
        {
            if (input[i] == '%')
            {
                if (i == 0)
                {
                    return parse_function_or_variable();
                }
                else
                {
                    string temp = input[0..i];
                    input = input[i..$];
                    return ParserResult.ok(new TextExpression(temp));
                }
            }
        }

        string temp = input;
        input = "";
        return ParserResult.ok(new TextExpression(temp));
    }

    ParserResult parse_function_or_variable()
    {
        input = input[1..$];

        for (int i = 0; i < input.length; ++i)
        {
            if (!(isAlphaNum(input[i]) || input[i] == '_'))
            {
                if (input[i] == '(')
                {
                    FunCallExpression function_call = new FunCallExpression();
                    function_call.function_name = input[0..i];
                    input = input[i+1..$];

                    // No arguments to the function call
                    if (input[0] == ')')
                    {
                        input = input[1..$];
                        return ParserResult.ok(function_call);
                    }

                    do
                    {
                        ParserResult arg_result = parse_function_argument();
                        if (arg_result.is_error)
                        {
                            return arg_result;
                        }

                        function_call.args.insert(arg_result.result);
                    } while(check_if_next_is_comma_and_drop_it());

                    if (input.empty)
                    {
                        return ParserResult.fail("Unexpected end of input after argument list");
                    }

                    if (input[0] != ')')
                    {
                        return ParserResult.fail("Expected ')' but got `" ~ [input[0]] ~ "` instead");
                    }

                    input = input[1..$];

                    return ParserResult.ok(function_call);
                }
                else
                {
                    if (i == 0)
                    {
                        return ParserResult.fail("Zero-length variable name");
                    }

                    string varname = input[0..i];
                    input = input[i..$];
                    return ParserResult.ok(new VariableExpression(varname));
                }
            }
        }

        string temp = input;
        input = "";
        return ParserResult.ok(new VariableExpression(temp));
    }

    ParserResult parse_function_argument()
    {
        input = input.strip;
        if (input.empty)
        {
            return ParserResult.fail("Unexpected end of input in function argument");
        }

        if (input[0] == '"')
        {
            return parse_string_literal();
        }
        else if (input[0] == '%')
        {
            return parse_function_or_variable();
        }
        else
        {
            return ParserResult.fail("Unexpected `" ~ [input[0]] ~ "`");
        }
    }

    ParserResult parse_string_literal()
    {
        if (input[0] != '"')
        {
            return ParserResult.fail("Expected `\"`");
        }
        else
        {
            for (int i = 1; i < input.length; ++i)
            {
                if (input[i] == '"')
                {
                    string literal = input[1..i];
                    input = input[i+1..$];
                    return ParserResult.ok(new TextExpression(literal));
                }
            }

            return ParserResult.fail("Unexpected end of input in string literal");
        }
    }

    bool check_if_next_is_comma_and_drop_it()
    {
        input = input.strip;

        if (input.empty)
        {
            return false;
        }

        if (input[0] == ',')
        {
            input = input[1..$];
            return true;
        }
        else
        {
            return false;
        }
    }
}
