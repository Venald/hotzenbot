/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.replcontext;

import hotzenbot.commands.context;
import hotzenbot.sys.result;

import d2sqlite3;

import std.stdio;

class ReplContext : Context
{
    string access_token;
    string client_id;

    this(Database* db, string access_token, string client_id)
    {
        this.database = db;
        this.access_token = access_token;
        this.client_id = client_id;
    }

    override void reply_to_sender(string message)
    {
        writeln(message);
    }

    override void reply(string message)
    {
        writeln(message);
    }

    override char call_prefix()
    {
        return '!';
    }

    override void log_command_call(int id, string args)
    {
        // No logging yet
    }

    override bool is_sender_authority()
    {
        return true;
    }

    override bool is_sender_trusted()
    {
        return true;
    }

    override bool is_sender_maintainer()
    {
        return true;
    }

    override bool is_bot_mentioned(string _)
    {
        return false;
    }

    override bool is_sender_ignored()
    {
        return false;
    }

    override Result!string set_calc_var(string _, double __)
    {
        return Result!string.fail("Cannot set calc variables in a REPL context");
    }

    override Result!double get_calc_var(string _)
    {
        return Result!double.fail("Cannot get calc variables in a REPL context");
    }

    override Result!string timeout_sender(uint secs)
    {
        return Result!string.fail("Cannot timeout user on the REPL");
    }
}
