/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.context;

import d2sqlite3.database;

import std.stdio;
import std.string;
import std.typecons;

import hotzenbot.commands.expression;
import hotzenbot.commands.command;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

abstract class Context
{
    string[string] variables;
    Database* database;

    abstract char call_prefix();
    abstract void log_command_call(int id, string args);
    abstract void reply_to_sender(string message);
    abstract void reply(string message);
    abstract bool is_bot_mentioned(string message);
    abstract bool is_sender_trusted();
    abstract bool is_sender_authority();
    abstract bool is_sender_maintainer();
    abstract bool is_sender_ignored();
    abstract Result!double get_calc_var(string var_name);
    abstract Result!string set_calc_var(string var_name, double val);
    abstract Result!string timeout_sender(uint seconds);

    final Nullable!int command_id(string command_name)
    {
        auto maybe_id = database.command_id(command_name);

        if (maybe_id.is_error)
        {
            log_error(LoggingScope.System, maybe_id.err_msg);
            return Nullable!int();
        }

        return Nullable!int(maybe_id.result);
    }

    final Nullable!uint times_command(string command_name)
    {
        auto times = database.command_times(command_name);

        if (times.is_error)
        {
            log_error(LoggingScope.System, times.err_msg);
            return Nullable!uint();
        }

        return Nullable!uint(cast(uint)times.result);
    }
}
