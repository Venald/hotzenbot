/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.irccontext;

import hotzenbot.bot.messagehandler;
import hotzenbot.bot.roles;
import hotzenbot.commands.context;
import hotzenbot.irc.ircwritethread;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.algorithm;
import std.string;
import std.conv : to;

import d2sqlite3;

import dialect;

class IRCContext : Context
{
    this(Database* database, string bot_nickname, string network, IRCEvent event, IRCWriteThread respond_thread, IRCNetworkState.ChannelState* channel_state)
    {
        // Prepopulate irc-specific variables
        this.variables["ichannel"] = event.channel[1..$];
        this.variables["sender"] = event.sender.nickname;
        this.variables["channeltopic"] = channel_state.topic;
        this.sender = event.sender.nickname;
        this.channel = event.channel;
        this.address = event.sender.address;
        this.database = database;
        this.write_thread = respond_thread;
        this.network = network;
        this.channel_state = channel_state;
        this.bot_nickname = bot_nickname;
    }

    string channel;
    string server;
    string sender;
    string address;
    string network;
    string bot_nickname;
    IRCWriteThread write_thread;
    IRCNetworkState.ChannelState* channel_state;

    override void reply(string message)
    {
        while (message.startsWith('/'))
        {
            message = message[1..$];
        }

        write_thread.push_message("PRIVMSG " ~ channel ~ " :" ~ message ~ "\r\n");
    }

    override void reply_to_sender(string message)
    {
        reply(sender ~ ": " ~ message);
    }

    override char call_prefix()
    {
        auto settings = database.hotzenbot_exec("SELECT channelCommandPrefix FROM JoinedIRCChannels WHERE name = :channel AND network = :network;",
                                                channel, network);
        if (settings.empty)
        {
            // TODO(#19): If no call prefix is set, initialize the database
            return '!';
        }

        return settings.front.peek!string(0)[0];
    }

    override void log_command_call(int id, string args)
    {
        try
        {
            database.hotzenbot_exec("INSERT INTO CommandLog (userIrcAddress, commandId, commandArgs) "~
                                    "VALUES (:userIrcAddress, :commandId, :commandArgs );",
                                    this.address, id, !args ? "" : args);
            log_debug(LoggingScope.IRC, "Logged irc command");
        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to log irc command:" ~ e.to!string);
        }
    }

    bool is_channel_owner()
    {
        foreach (nick; channel_state.nicks)
        {
            if (nick.startsWith("@") && nick[1..$] == this.sender)
                return true;
        }

        return false;
    }

    override bool is_sender_trusted()
    {
        auto roles = database.custom_roles_of_irc_user(network, address, sender);
        return roles[].any!"a.is_trusted" || this.is_channel_owner;
    }

    override bool is_sender_authority()
    {
        auto roles = database.custom_roles_of_irc_user(network, address, sender);
        return roles[].any!"a.is_authority" || this.is_channel_owner;
    }

    // TODO(#37): There is no way to make someone a maintainer on irc other than editing the database by hand
    override bool is_sender_maintainer()
    {
        auto roles = database.custom_roles_of_irc_user(network, address, sender);
        return roles[].any!"a.is_maintainer";
    }

    override bool is_bot_mentioned(string message)
    {
        return message.startsWith(this.bot_nickname ~ ":");
    }

    override bool is_sender_ignored()
    {
        auto roles = database.custom_roles_of_irc_user(network, address, sender);
        return roles[].any!"a.is_ignored";
    }

    override Result!double get_calc_var(string var_name)
    {
        try
        {
            auto results = database.hotzenbot_exec(
                "SELECT * FROM CalcVariables WHERE calcVarIrcNick = :userNick " ~
                "AND calcVarIrcAddress = :userAddress " ~
                "AND calcVarIrcNetwork = :userNetwork " ~
                "AND calcVarName = :varName;",
                sender, address, network, var_name);

            if (results.empty)
            {
                return Result!double.fail("Variable `" ~ var_name ~ "` is undefined.");
            }
            else
            {
                return Result!double.ok(results.front["calcVarValue"].as!double);
            }
        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to fetch variable `" ~ var_name ~ "`: " ~ e.to!string);
            return Result!double.fail("Unable to fetch variable `" ~ var_name ~ "`.");
        }
    }

    override Result!string set_calc_var(string var_name, double value)
    {
        try
        {
            database.hotzenbot_exec(
                "INSERT INTO CalcVariables (calcVarIrcNick, calcVarIrcAddress, calcVarIrcNetwork, calcVarName, calcVarValue) " ~
                "VALUES (:nick, :address, :network, :varName, :value);",
                sender, address, network, var_name, value);
            return Result!string.ok("Variable has been set.");
        }
        catch (Exception e)
        {
            log_error(LoggingScope.IRC, "Unable to set variable `" ~ var_name ~ "`: " ~ e.to!string);
            return Result!string.fail("Unable to set variable `" ~ var_name ~ "`.");
        }
    }

    // TODO(#61): Implement timeout on IRC
    override Result!string timeout_sender(uint secs)
    {
        return Result!string.fail("Cannot timeout user on IRC.");
    }
}
