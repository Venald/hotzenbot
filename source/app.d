/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

import core.stdc.stdlib;
import core.thread;
import std.algorithm;
import std.concurrency;
import std.exception;
import std.file;
import std.json;
import std.parallelism;
import std.stdio;

import hotzenbot.bot.markovthread;
import hotzenbot.bot.messagehandler;
import hotzenbot.bot.timerthread;
import hotzenbot.bot.gitlabhelp;
import hotzenbot.bot.globalsignal;
import hotzenbot.commands.context;
import hotzenbot.commands.expressionparser;
import hotzenbot.commands.pipe;
import hotzenbot.commands.replcontext;
import hotzenbot.irc.ircconnection;
import hotzenbot.irc.twitchconnection;
import hotzenbot.sys.logging;
import hotzenbot.sys.migrations;
import hotzenbot.sys.repl;
import hotzenbot.sys.transport;

import d2sqlite3.database;

void main(string[] args)
{
    init_logger();

    scope(exit) {
        kill_logger();
    }

    if (args.length == 4 && args[1] == "repl")
    {
        repl(args);
    }

    if (args.length != 3)
    {
        stderr.writeln("ERR : Invalid number of arguments provided\n" ~
                       "usage: ./hotzenbot <secret.json> <database.sqlite>");
        abort();
    }


    log_info(LoggingScope.System, "Reading config file");

    ThreadGroup bot_threads = new ThreadGroup();
    string secret_content = readText(args[1]);
    JSONValue config = parseJSON(secret_content);
    Database* database = new Database(args[2]);
    Transport[] bot_transports;

    Migrations migrations = new Migrations();
    migrations.check_and_apply_migrations(database);

    BotMessageHandler handler = new BotMessageHandler(database);
    MarkovThread markov_thread = new MarkovThread(database);
    ReplThread repl_thread = new ReplThread(database);
    TimerThread timer_thread = new TimerThread(database);
    GitLabThread gitlab_thread;

    if ("gitlab" in config)
    {
        gitlab_thread = new GitLabThread(database,
                                         config["gitlab"]["token"].str,
                                         config["gitlab"]["snippet_title"].str);
        gitlab_thread.start();
    }
    else
    {
        log_warn(LogginScope.System, "No Gitlab config provided");
    }

    bot_threads.add(markov_thread);
    bot_threads.add(timer_thread);
    // We don't wanna add the repl- and the gitlab-thread, since the both block
    bot_threads.each!(x => x.start());

    if ("twitch" in config)
    {
        TwitchConnection twitch_conn = new TwitchConnection(config, &handler);
        twitch_conn.connect();
        twitch_conn.auto_join_channels();

        repl_thread.add_transport_layer("twitch", twitch_conn);
        repl_thread.access_token = config["twitch"]["token"].str.dup;
        repl_thread.client_id = config["twitch"]["clientId"].str.dup;

        timer_thread.add_transport_layer("twitch", twitch_conn);
        bot_transports ~= twitch_conn;
    }
    else
    {
        log_warn(LogginScope.System, "No Twitch config provided");
    }

    if ("irc" in config)
    {
        foreach(net; config["irc"].array)
        {
            string network = net["network"].str;
            IRCConnection irc_conn = new IRCConnection(net, &handler, network);

            irc_conn.connect();
            irc_conn.auto_join_channels();
            repl_thread.add_transport_layer(network, irc_conn);

            timer_thread.add_transport_layer(network, irc_conn);

            bot_transports ~= irc_conn;
        }
    }

    repl_thread.start();

    update_gitlab_help_page();

    register(GLOBAL_THREAD_NAME, thisTid);
    bool should_exit = false;
    while (!should_exit) {
        receive(
            (GlobalSignal s) {
                final switch (s)
                {
                case GlobalSignal.Shutdown:
                    should_exit = true;
                    break;
                }
            }
        );
    }

    log_info(LoggingScope.System, "Shutting down the bot");
    repl_thread.request_exit();
    gitlab_thread.request_exit();
    markov_thread.request_exit();
    timer_thread.request_exit();
    log_info(LoggingScope.System, "Waiting for all transports to exit");
    bot_transports.each!(x => x.exit());
    log_info(LoggingScope.System, "Joining threads");
    bot_threads.joinAll();
    log_info(LoggingScope.System, "Closing the database");
    database.close();
    log_info(LoggingScope.System, "Shutdown succeeded.");
}

void repl(string[] args)
{
    Database* database = new Database(args[3]);

    string secret_content = readText(args[2]);
    JSONValue config = parseJSON(secret_content);

    MarkovThread markov_thread = new MarkovThread(database);
    markov_thread.start();

    if ("twitch" !in config)
    {
        stderr.writeln("ERR : Need a twitch config");
        return;
    }

    ReplContext ctx = new ReplContext(database, config["twitch"]["token"].str, config["twitch"]["clientId"].str);

    while(true)
    {
        write("> ");
        string foo = readln();
        auto pipe = Pipe.parse(ctx, foo);

        if (pipe.isNull)
        {
            writeln("Not a valid pipe");
            continue;
        }

        auto eval_res = pipe.get.run(ctx);

        if (eval_res.is_error)
            writeln(eval_res.err_msg);
        else
            writeln(eval_res.result);
    }
}
